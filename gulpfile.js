
/*
    Generic Gulp file for MODx projects.

    Version 1.01

    Created: 2018-06-19
    Last update: 2018-08-06

    Required files: package.json

    USAGE:
    gulp           >> Identical to gulp dev
    gulp acc       >> Identical to gulp prod
    gulp dev       >> If building on a local server, using browser sync. (Make sure a local proxy server is running),
    gulp test      >> If building on a development environment. Does not use browser sync.
    gulp prod      >> For production and acceptance environments. No watching of files.
    gulp default   >> Identical to gulp dev

*/


/*
    === Required libraries ========================================================================
*/

var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    sass = require('gulp-sass'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    cleancss = require('gulp-clean-css'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    cache = require('gulp-cache'),
    flatmap = require('gulp-flatmap'),
    requirejs = require('gulp-requirejs'),
    imagemin = require('gulp-imagemin');

var bs = require('browser-sync').create(); // Create a browser sync instance.

var develop = true; // Set default enviroment to develop


/* 
    === Configuration settings for project ========================================================
*/

var serverConfig = {
    templateServer: 'http://adwise.nl.local', // Proxy server used for development of frontend templates
}
var sassConfig = {
    inputDirectory: 'src/scss/**/*.scss',
    outputDirectory: 'public_html/static/default/style',
    options: {
        includePaths: ['src/scss/**/*.scss', 'node_modules/foundation-sites/scss/']
    }
}
var jsConfig = {
    inputDirectory: [
        'modernizr.js',
        'node_modules/jquery/dist/jquery.js',
	    'node_modules/foundation-sites/dist/js/plugins/foundation.core.js',
	    'node_modules/foundation-sites/dist/js/plugins/foundation.util.triggers.js',
        'node_modules/foundation-sites/dist/js/plugins/foundation.util.mediaQuery.js',
        'node_modules/foundation-sites/dist/js/plugins/foundation.sticky.js',
        'node_modules/foundation-sites/dist/js/plugins/foundation.interchange.js',
        'node_modules/foundation-sites/dist/js/plugins/foundation.magellan.js',
        'src/scripts/vendor/*.js',
        'src/scripts/custom/*.js'
    ],
    outputDirectory: 'public_html/static/default/script',
    options: {}
}
var imgConfig = {
    inputDirectory: [
        'src/images/**/*.jpg',
        'src/images/**/*.jpeg',
        'src/images/**/*.png',
        'src/images/**/*.gif',
	    'src/images/**/*.ico',
        'src/images/**/*.svg'
    ],
    outputDirectory: 'public_html/static/default/media/images',
    options: {}
}
var fontConfig = {
    inputDirectory: [
        'src/fonts/**/*.ttf',
        'src/fonts/**/*.otf',
        'src/fonts/**/*.woff',
        'src/fonts/**/*.woff2',
        'src/fonts/**/*.svg',
        'src/fonts/**/*.eot'
    ],
    outputDirectory: 'public_html/static/default/fonts',
    options: {}
}
var templateConfig = {
    inputDirectory: [
        'src/templates/*.php',
        'src/templates/*.html'
    ],
    outputDirectory: 'public_html/templates',
    options: {}
}
var faviconConfig = {
    inputDirectory: [
        'src/images/favicon/*.xml',
        'src/images/favicon/*.ico',
        'src/images/favicon/*.json'
    ],
    outputDirectory: 'public_html/static/default/media/images/favicon',
    options: {}
}


/* 
    === Gulp processing tasks =====================================================================
*/

// Parse scss
gulp.task('styles', function () {
    return gulp
        .src(sassConfig.inputDirectory)
        .pipe(gulpif(develop, sourcemaps.init()))
        .pipe(sass(sassConfig.options).on('error', sass.logError))
        .pipe(rename({ suffix: '.min' }))
        .pipe(cleancss())
        .pipe(gulpif(develop, sourcemaps.write('./', { includeContent: false })))
        .pipe(gulp.dest(sassConfig.outputDirectory))
        .pipe(bs.reload({ stream: true }));
});

var path = require('path');

//css task for theming
gulp.task('stylesTheming', function () {
    gulp.src('src/scss/themes*/')
        .pipe(flatmap((stream, dir) =>
            gulp.src(dir.path + '/**/*.scss')
                .pipe(sass({
                    outputStyle: 'compressed',
                }))
                .pipe(rename({suffix: '.min'}))
                .pipe(gulpif(develop, sourcemaps.write('./', {includeContent: false})))
                .pipe(gulp.dest('public_html/static/default/style/' + path.basename(dir.path)))
        ))
});

// Handle javascript
gulp.task('scripts', function () {
    return gulp
        .src(jsConfig.inputDirectory)
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(sourcemaps.write('./', { includeContent: false }))
        .pipe(gulp.dest(jsConfig.outputDirectory))
        .pipe(bs.reload({ stream: true }));
});

// Handle images
gulp.task('images', function () {
    return gulp.src(imgConfig.inputDirectory)
        .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
        .pipe(gulp.dest(imgConfig.outputDirectory))
        .pipe(bs.reload({ stream: true }));
});

// Copy fonts
gulp.task('fonts', function () {
    return gulp.src(fontConfig.inputDirectory)
        .pipe(gulp.dest(fontConfig.outputDirectory))
        .pipe(bs.reload({ stream: true }));
});

// Copy templates
gulp.task('templates', function () {
    return gulp.src(templateConfig.inputDirectory)
        .pipe(gulp.dest(templateConfig.outputDirectory))
        .pipe(bs.reload({ stream: true }));
});

// Copy favicon stuff
gulp.task('favicons', function () {
    return gulp.src(faviconConfig.inputDirectory)
        .pipe(gulp.dest(faviconConfig.outputDirectory))
        .pipe(bs.reload({ stream: true }));
});


/* 
    === Gulp other tasks ==========================================================================
*/

// Browser sync for templates
gulp.task('browser-sync-templates', ['styles', 'scripts', 'images', 'fonts', 'templates', 'favicons'], function () {
    bs.init({
        proxy: {
            target: serverConfig.templateServer + "/templates/", // can be [virtual host, sub-directory, localhost with port]
            ws: true // enables websockets
        }
    });
});

// Browser sync for normal development
gulp.task('browser-sync-develop', ['styles', 'scripts', 'images', 'fonts', 'templates', 'favicons'], function () {
    bs.init({
        proxy: {
            target: serverConfig.developmentServer, // can be [virtual host, sub-directory, localhost with port]
            ws: true // enables websockets
        }
    });
});

// Set environment to production
gulp.task('set-production', () => {
    develop = false;
});

/* 
    === Gulp main tasks ===========================================================================
*/

// gulp dev
//
// Local development
// Use this for building frontend templates on a local dev environment
gulp.task('dev', ['browser-sync-templates'], function () {
    gulp.watch([sassConfig.inputDirectory], ['styles']);
    gulp.watch([jsConfig.inputDirectory], ['scripts']);
    gulp.watch([imgConfig.inputDirectory], ['images']);
    gulp.watch([fontConfig.inputDirectory], ['fonts']);
    gulp.watch([templateConfig.inputDirectory], ['templates']);
    gulp.watch([faviconConfig.inputDirectory], ['favicons']);
});

// gulp test
//
// Remote or local development (development, test)
// Identical to 'dev', but no proxy
gulp.task('test', ['styles', 'scripts', 'images', 'fonts', 'favicons'], function () {
    gulp.watch([sassConfig.inputDirectory], ['styles']);
    gulp.watch([jsConfig.inputDirectory], ['scripts']);
    gulp.watch([imgConfig.inputDirectory], ['images']);
    gulp.watch([fontConfig.inputDirectory], ['fonts']);
    gulp.watch([templateConfig.inputDirectory], ['templates']);
    gulp.watch([faviconConfig.inputDirectory], ['favicons']);
});

gulp.task('theming', ['stylesTheming', 'scripts', 'images', 'fonts', 'favicons'], function () {
    gulp.watch([sassConfig.inputDirectory], ['stylesTheming']);
    gulp.watch([jsConfig.inputDirectory], ['scripts']);
    gulp.watch([imgConfig.inputDirectory], ['images']);
    gulp.watch([fontConfig.inputDirectory], ['fonts']);
    gulp.watch([faviconConfig.inputDirectory], ['favicons']);
});

// gulp prod
//
// Gulp production (run once for production)
// No watching, just process once. Skip the templates directory
gulp.task('prod', ['set-production', 'styles', 'scripts', 'fonts', 'favicons']);

// gulp acc
//
// Identical to gulp prod
gulp.task('acc', ['prod']);

// gulp default
//
// Identical to gulp dev
gulp.task('default', ['dev']);


/*
    ===============================================================================================
*/