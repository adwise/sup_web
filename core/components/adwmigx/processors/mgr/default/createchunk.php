<?php

opcache_reset();

if (!isset($_POST['objects'])) {
    exit('no ids');
}

$config   = $modx->migx->customconfigs;
$prefix   = isset($config['prefix']) && !empty($config['prefix']) ? $config['prefix'] : null;
$errormsg = '';
if (isset($config['use_custom_prefix']) && !empty($config['use_custom_prefix'])) {
    $prefix = isset($config['prefix']) ? $config['prefix'] : '';
}
$packageName  = 'migx';
$packagepath  = $modx->getOption('core_path').'components/'.$packageName.'/';
$modelpath    = $packagepath.'model/';
$is_container = $modx->getOption('is_container', $config, false);
if (is_dir($modelpath)) {
    $modx->addPackage($packageName, $modelpath, $prefix);
}
$configs   = $modx->getOption('configs', $scriptProperties, '');
$classname = $modx->getOption('classname', $config, '');

$c = $modx->newQuery('migxConfig');
$c->sortby($sort, $dir);
$c->limit($limit, $start);
$c->where(['id:IN' => explode(',', $_POST['objects'])]);
$c->prepare();
//error_log($c->toSQL()); // <-- uncomment for debugging

$pages = $modx->getCollection('migxConfig', $c);

//echo $output;

//exit();
foreach ($pages as $page) {
    $name = $page->name;

    if ($name == '') {
        continue;
    }
    $extended = json_decode($page->extended);

    $menuName = $extended->multiple_formtabs_optionstext;
    //exit();
    $fields = "{ignore]\r\n".'Menu naam = '.$menuName."\r\n";
    $page   = json_decode($page->formtabs);

    $fields .= 'Alle velden in deze migx:'."\r\n";
    foreach ($page as $key => $tab) {

        //print_r($tab);
        $newArray = $tab->fields;
        foreach ($newArray as $field) {
            $fields .= '{{$block.'.$field->field.'}} ('.$field->inputOptionValues.')'."\r\n";
        }
    }

    $fields    .= '{/ignore}'."\r\n";
    $filePath = $modx->config['elements_path'].'chunk/blocks/'.$name.'.tpl';

    if (!is_file($filePath)) {
        $contents = 'This is a test!';           // Some simple example content.
        if ($fileAdd = file_put_contents($filePath, $fields)) {     // Save our content to the file.
            print '{"msg":"file '.$filePath.' created?"}';
        } else {
            print '{"msg":"file '.$filePath.' something went wrong, wrong path?"}';
        }
    } else {
        print '{"msg":"file '.$filePath.' already exists"}';
    }

}
//error_log(print_r($list,true));  // <-- another debugging point

@session_write_close();
exit();

opcache_reset();

if (!isset($_POST['objects'])) {
    exit('no ids');
}

$config   = $modx->migx->customconfigs;
$prefix   = isset($config['prefix']) && !empty($config['prefix']) ? $config['prefix'] : null;
$errormsg = '';
if (isset($config['use_custom_prefix']) && !empty($config['use_custom_prefix'])) {
    $prefix = isset($config['prefix']) ? $config['prefix'] : '';
}
$packageName  = 'migx';
$packagepath  = $modx->getOption('core_path').'components/'.$packageName.'/';
$modelpath    = $packagepath.'model/';
$is_container = $modx->getOption('is_container', $config, false);
if (is_dir($modelpath)) {
    $modx->addPackage($packageName, $modelpath, $prefix);
}
$configs   = $modx->getOption('configs', $scriptProperties, '');
$classname = $modx->getOption('classname', $config, '');

$c = $modx->newQuery('migxConfig');
$c->sortby($sort, $dir);
$c->limit($limit, $start);
$c->where(['id:IN' => explode(',', $_POST['objects'])]);
$c->prepare();
//error_log($c->toSQL()); // <-- uncomment for debugging

$pages = $modx->getCollection('migxConfig', $c);

//echo $output;

//exit();
foreach ($pages as $page) {
    $name = $page->name;

    if ($name == '') {
        continue;
    }
    $extended = json_decode($page->extended);

    $menuName = $extended->multiple_formtabs_optionstext;
    //exit();
    $fields = "{ignore]\r\n".'Menu naam = '.$menuName."\r\n";
    $page   = json_decode($page->formtabs);

    $fields .= 'Alle velden in deze migx:'."\r\n";
    foreach ($page as $key => $tab) {

        //print_r($tab);
        $newArray = $tab->fields;
        foreach ($newArray as $field) {
            $fields .= '{{$block.'.$field->field.'}} ('.$field->inputOptionValues.')'."\r\n";
        }
    }

    $fields   .= '{/ignore}'."\r\n";
    $filePath = $modx->config['elements_path'].'chunk/blocks/'.$name.'.tpl';

    if (!is_file($filePath)) {
        $contents = 'This is a test!';           // Some simple example content.
        if ($fileAdd = file_put_contents($filePath, $fields)) {     // Save our content to the file.
            print '{"msg":"file '.$filePath.' created?"}';
        } else {
            print '{"msg":"file '.$filePath.' something went wrong, wrong path?"}';
        }
    } else {
        print '{"msg":"file '.$filePath.' already exists"}';
    }

}
//error_log(print_r($list,true));  // <-- another debugging point

@session_write_close();
exit();
