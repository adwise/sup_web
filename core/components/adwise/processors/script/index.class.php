<?php

class adwScriptProcessor extends modObjectProcessor {

	/** @var $modx modX */
	public $modx = null;

	/** @var $adwise Adwise */
	public $adwise = null;

	public function __construct(modX & $modx, array $properties = array()) {
		parent::__construct($modx, $properties);

		$this->adwise =& $modx->adwise;
	}

	public function initialize() {
		header('Content-type: text/plain; charset: UTF-8');

		$this->modx->setLogTarget('ECHO');
		$this->modx->setLogLevel(Modx::LOG_LEVEL_DEBUG);

		return parent::initialize();
	}

	public function process() {
		$call = $this->adwise->option('call', $_GET, false);
		if (!empty($call)) {
			$file = str_replace(
				array(
					DIRECTORY_SEPARATOR,
					'.',
					DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR
				),
				array(
					'',
					DIRECTORY_SEPARATOR,
					DIRECTORY_SEPARATOR
				),
				$call
			);

			if (file_exists($this->getScriptsPath() . $file . '.php')) {
				$modx =& $this->modx;
				$adwise =& $this->adwise;
				$script =& $this;

				include $this->getScriptsPath() . $file . '.php';
			}
			else
				$this->log('Script not found: ' . $call);
		}
	}

	public function __destruct() {
		$this->log('Done!', __LINE__, __FILE__);
		exit();
	}

	protected function getScriptsPath() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR . 'calls' . DIRECTORY_SEPARATOR;
	}

	public function log($message, $line = __LINE__, $file = null, $level = Modx::LOG_LEVEL_INFO) {
		$line = str_pad($line, 3, '0', STR_PAD_LEFT);

		if (is_null($file))
			$file = $this;

		if (is_object($file)) {
			$file = get_class($file);
		}
		else {
			$file = substr(strtr(realpath($file), '\\', '/'), strlen(dirname(__FILE__)) + 1);
		}

		$this->modx->log($level, $message, 'ECHO', get_class($this->adwise) . '::script', empty($file) ? 'execution' : $file, $line);

		return true;
	}
}

return 'adwScriptProcessor';