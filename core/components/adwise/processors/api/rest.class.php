<?php

class REST {

	/**
	 * The main MODX object reference.
	 *
	 * @var     modX $modx
	 * @since  1.0.0-alpha1
	 * @access public
	 */
	private $modx = null;

	/**
	 * The main Adwise object reference.
	 *
	 * @var     Adwise $adw
	 * @since  1.0.0-alpha1
	 * @access public
	 */
	public $adw = null;

	protected $encrypted = false;

	protected $mcryptPassphrase = 'Just some meaningless string.. ;p';

	protected $contentType = 'application/json';

	protected $_function = null;

	protected $_request = null;

	public function __construct() {
		$this->_request = $this->encrypted ? $this->decrypt($_REQUEST) : $_REQUEST;
	}

	public function initialize(modX & $modx) {
		$this->modx =& $modx;
		$this->adw =& $this->modx->adwise;
	}

	public function process() {
		$function = $this->adw->option('call', $_REQUEST, null);

		$this->_function = $function;

		if (method_exists($this, $function)) {
			$this->$function();
		}
		else {
			$this->response(
				array(
					'status' => 'error',
					'message' => 'function not implemented (' . get_class($this) . ')'
				),
				406
			);
		}
	}

	protected function request($key, $default = false, $skip = true, $replace = false) {
		return $this->adw->option($key, $this->_request, $default, $skip, $replace);
	}

	public function update($silent = false) {
		require_once(dirname(__FILE__) . '/bitbucket.class.php');

		$bitbucket = new Bitbucket('adwise', 'adwise_api');
		$bitbucket->setCredentials($this->request('username'), $this->request('password'));

		$tags = $bitbucket->getTags();

		if (!empty($tags)) {
			$tag = $tags[0];
			$download = sys_get_temp_dir() . '/adwise_api_' . $tag['node'] . '.tar.bz2';
			$extractDir = sys_get_temp_dir() . '/adwise_api_' . $tag['node'];

			$bitbucket->downloadNode($tag['node'], $download);
			$dumpDir = $bitbucket->extractDownload($download, $extractDir);

			if (is_dir($dumpDir) && file_exists($dumpDir . '/connections/modx.class.php')) {
				exec('rm -rf ' . dirname(__FILE__) . '/modx.class.php');
				exec('mv ' . $dumpDir . '/connections/modx.class.php ' . dirname(__FILE__));
				exec('rm -rf ' . $download . ' ' . $extractDir);

				if (!$silent)
					$this->response(
						array(
							'status' => 'ok',
							'message' => 'Updated to ' . $tag['name'] . ' (' . $tag['node'] . ')'
						)
					);
			}
			else {
				if (!$silent)
					$this->response(
						array(
							'status' => 'ok',
							'message' => 'Error while updating' . (!empty($tag) ? ' to ' . $tag['name'] . ' (' . $tag['node'] . ')' : '')
						)
					);
			}
		}
		else {
			if (!$silent)
				$this->response(
					array(
						'status' => 'ok',
						'message' => 'Error while updating (could not retrieve tags)'
					)
				);
		}
	}

	protected function response($data, $status = false) {
		$this->setHeaders($status ? $status : 200);

		if (is_array($data))
			$data['function'] = $this->_function;

		exit($this->modx->toJSON($this->encrypted ? $this->encrypt($data) : $data));
	}

	protected function setHeaders($code) {
		header("HTTP/1.1 " . $code . " " . $this->getStatusMessage($code));
		header("Content-Type:" . $this->contentType);
	}

	protected function getStatusMessage($code) {
		$status = array(
			100 => 'Continue',
			101 => 'Switching Protocols',
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			306 => '(Unused)',
			307 => 'Temporary Redirect',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported'
		);

		return isset($status[$code]) ? $status[$code] : $status[500];
	}

	protected function encrypt($str, $key = null) {
		if (empty($key))
			$key = $this->mcryptPassphrase;

		if (is_array($str)) {
			$encryptedArray = array();
			foreach($str as $k => $v) {
				$encryptedArray[$this->encrypt($k, $key)] = $this->encrypt($v, $key);
			}

			return $encryptedArray;
		}
		else {
			$block = mcrypt_get_block_size('des', 'ecb');
			$pad = $block - (strlen($str) % $block);
			$str .= str_repeat(chr($pad), $pad);

			return base64_encode(@mcrypt_encrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB));
		}
	}

	protected function decrypt($str, $key = null) {
		if (empty($key))
			$key = $this->mcryptPassphrase;

		if (is_array($str)) {
			$decryptedArray = array();
			foreach($str as $k => $v) {
				$decryptedArray[$this->decrypt($k, $key)] = $this->decrypt($v, $key);
			}

			return $decryptedArray;
		}
		else {
			$str = @mcrypt_decrypt(MCRYPT_DES, $key, base64_decode($str), MCRYPT_MODE_ECB);

			$block = mcrypt_get_block_size('des', 'ecb');
			$pad = ord($str[($len = strlen($str)) - 1]);

			return substr($str, 0, strlen($str) - $pad);
		}
	}

	public function __destruct() {
		exit(PHP_EOL . PHP_EOL);
	}

}

return 'adwApiProcessor';