<?php

class adwApiProcessor extends modObjectProcessor {

	/**
	 * The main MODX object reference.
	 *
	 * @var     modX $modx
	 * @since  1.0.0-alpha1
	 * @access public
	 */
	public $modx = null;

	public function __construct(modX & $modx, array $properties = array()) {
		parent::__construct($modx, $properties);

		require_once(dirname(__FILE__) . '/rest.class.php');

		if (file_exists(dirname(__FILE__) . '/modx.class.php')) {
			require_once(dirname(__FILE__) . '/modx.class.php');
			$this->rest = new modxREST($this->modx);
		}
		else {
			$this->rest = new REST();
		}
		$this->rest->initialize($this->modx);
	}

	public function process() {
		$this->rest->process();
	}

	public function __destruct() {
		exit(PHP_EOL . PHP_EOL);

	}
}

return 'adwApiProcessor';