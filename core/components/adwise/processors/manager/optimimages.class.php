<?php

class adwOptimImagesProcessor extends modObjectProcessor {

	/** @var Adwise $adwise */
	public $adwise = null;

	/** @var adwiseImageOptim $oOptim */
	private $oOptim = null;

	public function __construct(modX & $modx, array $properties = array()) {
		parent::__construct($modx, $properties);

		$this->adwise =& $this->modx->adwise;

		$this->oOptim = $this->adwise->initClass('adwiseImageOptim');
	}

	public function process() {
		$this->modx->log(MODX_LOG_LEVEL_INFO, '[Adwise Image Optimiser] Optimising images...');

		$this->_processDirectory(rtrim($this->adwise->option(Adwise::alias .'.path_static'), DIRECTORY_SEPARATOR));

		$this->modx->log(MODX_LOG_LEVEL_INFO, 'Done...');

		sleep(2);

		return $this->success(
			'',
			array()
		);
	}

	private function _processDirectory($sPath) {
		$aImages = array();
		$aDirectories = array();

		$aFiles = scandir($sPath);
		foreach($aFiles as $sFile) {
			if ($sFile != '.' && $sFile != '..') {
				if (is_dir($sPath . DIRECTORY_SEPARATOR . $sFile)) {
					$aDirectories[] = $sPath . DIRECTORY_SEPARATOR . $sFile;
				}

				$sExt = pathinfo($sFile, PATHINFO_EXTENSION);
				if (in_array(
					$sExt,
					array(
						'jpg',
						'jpeg',
						'png',
						'gif',
						'svg'
					)
				)) {
					$aImages[] = $sPath . DIRECTORY_SEPARATOR . $sFile;
				}
			}
		}

		if (!empty($aImages))
			$this->modx->log(MODX_LOG_LEVEL_INFO, substr($sPath, strlen($this->adwise->option(Adwise::alias .'.path_static'))));

		foreach($aImages as $sImage) {
			$bResult = $this->oOptim->optimise($sImage);
			$this->modx->log(
				$bResult === true ? MODX_LOG_LEVEL_WARN : ($bResult === false ? MODX_LOG_LEVEL_ERROR : MODX_LOG_LEVEL_INFO),
				'-> ' . substr($sImage, strlen($sPath) + 1)
			);

		}

		foreach($aDirectories as $sDirectory) {
			$this->_processDirectory($sDirectory);
		}
	}
}

return 'adwOptimImagesProcessor';