<?php

class adwClearHistoryProcessor extends modObjectProcessor {

	/** @var Adwise $adwise */
	public $adwise = null;

	/** @var adwiseImageOptim $oOptim */
	private $oOptim = null;

	public function __construct(modX & $modx, array $properties = array()) {
		parent::__construct($modx, $properties);

		$this->adwise =& $this->modx->adwise;

		$this->oOptim = $this->adwise->initClass('adwiseImageOptim');
	}

	public function process() {
		$this->modx->log(MODX_LOG_LEVEL_INFO, '[Adwise Clear History] Clearing optimisation history...');

		$this->_processDirectory(rtrim($this->adwise->option(Adwise::alias .'.path_static'), DIRECTORY_SEPARATOR));

		$this->modx->log(MODX_LOG_LEVEL_INFO, 'Done...');

		sleep(2);

		return $this->success(
			'',
			array()
		);
	}

	private function _processDirectory($sPath) {
		$sHistoryFile = $sPath . DIRECTORY_SEPARATOR . '.imageoptim';

		if (file_exists($sHistoryFile)) {
			$aHistory = json_decode(file_get_contents($sHistoryFile), true);
			if (!is_array($aHistory))
				$aHistory = array();

			foreach($aHistory as $sKey => $bValue) {
				if ($bValue !== false || !file_exists($sPath . DIRECTORY_SEPARATOR . $sKey))
					unset($aHistory[$sKey]);
			}

			if (!empty($aHistory))
				file_put_contents($sHistoryFile, json_encode($aHistory));
			else
				unlink($sHistoryFile);
		}

		$aFiles = scandir($sPath);
		foreach($aFiles as $sFile) {
			if ($sFile != '.' && $sFile != '..') {
				if (is_dir($sPath . DIRECTORY_SEPARATOR . $sFile)) {
					$this->_processDirectory($sPath . DIRECTORY_SEPARATOR . $sFile);
				}
			}
		}
	}
}

return 'adwClearHistoryProcessor';