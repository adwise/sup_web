<?php
$field = $modx->getOption('field', $scriptProperties, false);
$isTV = $modx->getOption('isTV', $scriptProperties, '0');
$returnField = $modx->getOption('returnField', $scriptProperties, null);
$homeFallback = $modx->getOption('homeFallback', $scriptProperties, '0');

$resource = $modx->resource;

if (!isset($GLOBALS['get.inherited']))
	$GLOBALS['get.inherited'] = array();

if (!isset($GLOBALS['get.inherited'][$resource->get('id')])) {
	$children = $modx->getParentIds($resource->get('id'));
	if ($children[sizeof($children) - 1] == 0) {
		if ($homeFallback)
			$children[sizeof($children) - 1] = $modx->getOption('site_start');
		else
			unset($children[sizeof($children) - 1]);
	}
	array_unshift($children, $resource->get('id'));

	$GLOBALS['get.inherited'][$resource->get('id')] = $children;
}
else
	$children = $GLOBALS['get.inherited'][$resource->get('id')];

$query = $modx->newQuery('modResource', array('id:IN' => $children));
$query->sortby('FIELD(modResource.id, ' . implode(',', $children) . ')', 'ASC');
$query->limit(1);

if ($isTV) {
	$query->innerJoin('modTemplateVarResource', 'TemplateVarResources');
	$query->innerJoin('modTemplateVar', 'TemplateVar', 'TemplateVarResources.tmplvarid = TemplateVar.id');
	$query->where(
		array(
			'TemplateVar.name' => $field,
			array(
				"TemplateVarResources.value != '' AND TemplateVarResources.value != '[]'"
			)
		)
	);
}
else {
	$query->where(array($field . " != ''"));
}

$resource = $modx->getObject('modResource', $query);

return !empty($returnField) ? $resource->get($returnField) : ($isTV ? $resource->getTVValue($field) : $resource->get($field));