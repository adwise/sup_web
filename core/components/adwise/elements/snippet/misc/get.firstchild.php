<?php
$resource = $modx->getOption('resource', $scriptProperties, $modx->resource->get('id'));

$query = $modx->newQuery(
	'modResource',
	array(
		'parent' => $resource,
		'published' => 1
	)
);

$query->sortby('menuindex', 'ASC');
$query->limit(1);

$children = $modx->getIterator('modResource', $query);
foreach($children as $child) {
	return $child->get('id');
}

return;