<?php
/** @var Adwise $adwise */
$adwise =& $modx->adwise;

$aData = $adwise->option('input:fromjson', $scriptProperties, false);
$sTpl = $adwise->option('options', $scriptProperties, false);

$sOutput = '';
foreach($aData as $aRow) {
	$sOutput .= $modx->getChunk($sTpl, $aRow);
}

return $sOutput;