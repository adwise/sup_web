<?php
/** @var modX $modx */
/** @var array $scriptProperties */
/** @var array $sp */
$sp =& $scriptProperties;

if (empty($sp['type']) || !is_string($sp['type']))
	return '';
if ($sp['plain'])
	header('Content-type: text/plain; charset: utf-8');

switch(strtolower($sp['type'])) {
	case 'req':
	case 'request':
		echo '$_REQUEST dump at ' . date('d-m-Y H:i:s');
		echo $sp['plain'] ? "\n\n" : '<br /><br /><pre>';
		print_r($sp);
		print_r($_REQUEST);
		break;
	case 'pls':
	case 'placeholders':
		echo '$placeholders dump at ' . date('d-m-Y H:i:s');
		echo $sp['plain'] ? "\n\n" : '<br /><br /><pre>';
		print_r($sp);
		print_r($modx->placeholders);
		break;
	case 'lexi':
	case 'lexicons':
		$sp['prefix'] = $this->option('prefix', $sp, '');
		$sp['removePrefix'] = $this->option('removePrefix:bool', $sp, false);
		$sp['language'] = $this->option('language:lower', $sp, 'en');

		echo '$lexicons dump at ' . date('d-m-Y H:i:s');
		echo $sp['plain'] ? "\n\n" : '<br /><br /><pre>';
		print_r($sp);
		print_r($modx->lexicon->fetch($sp['prefix'], $sp['removePrefix'], $sp['language']));
		break;
}

echo $sp['plain'] ? "\n\n" : '</pre><br />';

if ($sp['exit'])
	exit();