<?php
/** @var modHashing $oHashing */
$oHashing = $modx->getService('hashing', 'hashing.modHashing');

/** @var modPBKDF2 $oHasher */
$oHasher = $oHashing->getHash(
	join(
		'&',
		array(
			microtime(true),
			uniqid(Adwise::alias, true),
			'The God Particle :p'
		)
	),
	'hashing.modPBKDF2'
);
$sHash = trim($oHasher->hash(microtime(true), array('salt' => $modx->uuid)), '=|');

if (!isset($_SESSION['csrf']))
	$_SESSION['csrf'] = array();

array_push(
	$_SESSION['csrf'],
	array(
		'hash' => $sHash,
		'generated' => time()
	)
);

while(sizeof($_SESSION['csrf']) > 10) {
	array_shift($_SESSION['csrf']);
}

return $sHash;