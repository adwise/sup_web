<!--@f:off-->
# Enforce [[+www:eq=`0`:then=`non-`]]www [[+ssl:eq=`1`:then=`SSL/HTTPS rewriting`:else=`rewriting`]] for domain "[[+url]]"
RewriteCond %{HTTP_HOST} .
RewriteCond %{HTTP_HOST} [[+url:replace=`.==\.`]]$ [NC]
RewriteCond %{HTTP_HOST} [[+www:eq=`1`:then=`!`]]^www\. [NC]
RewriteRule (.*) http[[+ssl:eq=`1`:then=`s`]]://[[+www:eq=`1`:then=`www.`]][[+url]]/$1 [R=301,QSA,L]
<!--@f:on-->