<!--@f:off-->
# Enforce [[+ssl:eq=`0`:then=`non-`]]SSL/HTTPS protocol for domain "[[+url]]"
RewriteCond %{REQUEST_URI} !^/manager
RewriteCond %{HTTP_HOST} [[+url:replace=`.==\.`]]$ [NC]
RewriteCond %{SERVER_PORT} [[+ssl:eq=`1`:then=`!`]]^443
RewriteRule (.*) http[[+ssl:eq=`1`:then=`s`]]://%{HTTP_HOST}/$1 [R=301,L]
<!--@f:on-->