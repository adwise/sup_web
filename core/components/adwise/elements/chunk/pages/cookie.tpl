<h2>Wat is een cookie?</h2>
<p>
	Wij maken op deze website gebruik van cookies. Een cookie is een eenvoudig klein bestandje dat met pagina’s van deze website en/of Flash-applicaties wordt meegestuurd en door uw browser op uw harde schrijf van uw computer wordt opgeslagen. De daarin opgeslagen informatie kan bij een volgend bezoek weer naar onze servers teruggestuurd worden. Deze cookies beschadigen uw computer niet.
</p><h3>[[++adw.cookielaw_name]] maakt gebruikt de volgende cookies:</h3>
<p>
	First party cookies:
</p>
<ul>
	<li>Google Analytics (analytische cookies)</li>
	[[++adw.cookielaw_technical:eq=`1`:then=`<li>Functionele en technische cookies (permanente en sessiecookies)</li>`]]
	<li>Overig: cookie control cookies</li>
</ul>
<p>
	Third-party cookies (cookies van derden):
</p>
<ul>
	<li>Advertentieprogramma’s</li>
	<li>Social Media</li>
</ul>
<h3>1. First Party Cookies</h3>
<h4>1.1 Google Analytics (analytische cookies)</h4>
<p>
	Via onze website wordt een cookie geplaatst van het bedrijf Google, als deel van de “Analytics”-dienst. Wij gebruiken deze dienst om bij te houden en rapportages te krijgen over hoe bezoekers de website gebruiken. Google kan deze informatie aan derden verschaffen indien Google hiertoe wettelijk wordt verplicht, of voor zover derden de informatie namens Google verwerken. Wij hebben hier geen invloed op. [[++adw.cookielaw_analytics:neq=`1`:then=`Wij hebben Google niet toegestaan de verkregen Analytics informatie te gebruiken voor andere Google diensten.`]]
</p>
<p>
	De informatie die Google verzamelt wordt zo veel mogelijk geanonimiseerd. Uw IP-adres wordt nadrukkelijk niet meegegeven. De informatie wordt overgebracht naar en door Google opgeslagen op servers in de Verenigde Staten. Google stelt zich te houden aan de Safe Harbor principles en is aangesloten bij het Safe Harbor-programma van het Amerikaanse Ministerie van Handel. Dit houdt in dat er sprake is van een passend beschermingsniveau voor de verwerking van eventuele persoonsgegevens.
</p>

[[++adw.cookielaw_technical:eq=`1`:then=`
	<h3>1.2 Functionele en technische cookies (noodzakelijke cookies)</h3>
	<p>
		Technische cookies zijn cookies die noodzakelijk zijn om de website(s) van [[++adw.cookielaw_name]] te laten functioneren, om een account aan te maken, om te kunnen inloggen en om fraude met uw account te detecteren. Functionele cookies onthouden uw voorkeuren. Dit betreft bij [[++adw.cookielaw_name]] onder meer [[++adw.cookielaw_technical_usage]].
	</p>

	<h4>1.2.1 Gebruik van sessiecookies</h4>
	[[++adw.cookielaw_sessioncookies:eq=`1`:then=`
		<p>
			Veel van de cookies die [[++adw.cookielaw_name]] gebruikt zijn sessiecookies. Met behulp van een sessie slaan we statusgegevens en voorkeuren op gedurende een bepaalde sessie. Wij kunnen onze dienst daardoor zoveel mogelijk aanpassen op het surfgedrag van onze bezoekers en het gebruiksgemak van de website verhogen tijdens de sessie. Denk hierbij aan het ingelogd blijven tijdens een sessie, uw [[++adw.cookielaw_sessioncookies_usage]]. Deze cookies worden automatisch verwijderd zodra u uw webbrowser afsluit.
		</p>
		<p>
			Let op: al deze cookies gebruiken we voor een goede functionaliteit van de website en garandeert verhoogd gebruiksgemak.
		</p>
	`:else=`
		<p>
			[[++adw.cookielaw_name]] maakt geen gebruik van sessiecookies.
		</p>
	`]]

	<h4>1.2.2 Gebruik van permanente cookies</h4>
	[[++adw.cookielaw_permanentcookies:eq=`1`:then=`
		<p>
			Met behulp van een permanente cookie herkennen we u bij een nieuw bezoek op onze website. De website kan daardoor speciaal op uw voorkeuren worden ingesteld. Ook wanneer u toestemming hebt gegeven voor het plaatsen van cookies kunnen wij dit door middel van een cookie onthouden (bijvoorbeeld een cookietool). Hierdoor hoeft u niet steeds uw voorkeuren te herhalen waardoor u dus tijd bespaart en een prettiger gebruik van onze website kunt maken. Permanente cookies kunt u verwijderen via de instellingen van uw browser.
		</p>
	`:else=`
		<p>
			[[++adw.cookielaw_name]] maakt geen gebruik van permanente cookies.
		</p>
	`]]

	<h4>1.2.3 Overig: cookie control cookies</h4>
	[[++adw.cookielaw_cookiecontrol:eq=`1`:then=`
		<p>
			Deze cookies worden geplaatst om uw voorkeuren met betrekking tot cookies te onthouden. Deze zijn uiteraard noodzakelijk om te weten of u wel of geen toestemming geeft voor het opslaan van de volgende cookies op uw apparatuur:
		</p>
		<ul>
			<li>Google Analytics (statistieken)</li>
			<li>Cookies van advertentieprogramma’s</li>
			<li>Social Media cookies</li>
		</ul>
	`:else=`
		<p>
			[[++adw.cookielaw_name]] maakt geen gebruik van cookie control cookies.
		</p>
	`]]
`]]

<h3>2. Third Party Cookies</h3>
<h4>2.1 Tracking cookies om advertenties beter af te stemmen op uw voorkeuren</h4>
<p>
	Met uw toestemming plaatsen andere partijen “tracking cookies” op uw apparatuur om advertenties beter af te stemmen op uw voorkeuren. Deze cookies gebruiken zij om bij te houden welke pagina’s u bezoekt buiten hun netwerk, om zo een profiel op te bouwen van uw online surfgedrag. Dit profiel wordt mede opgebouwd op basis van vergelijkbare informatie die zij van uw bezoek aan andere websites uit hun netwerk krijgen. Dit profiel wordt
	<strong>niet gekoppeld</strong> aan uw naam, adres, e-mailadres en dergelijke zoals bij ons bekend, maar dient alleen om advertenties af te stemmen op uw profiel zodat deze zo veel mogelijk relevant voor u zijn.
</p>

<h4>2.2 Social Media</h4>
<p>
	Op onze website zijn buttons opgenomen om webpagina’s te kunnen promoten (“liken”) of delen (“tweeten”) op sociale netwerken als Facebook en Twitter. Deze buttons werken door middel van stukjes code die van Facebook respectievelijk Twitter zelf afkomstig zijn. Door middel van deze code worden cookies geplaatst. Wij hebben daar geen invloed op. Leest u de privacyverklaring van Facebook respectievelijk van Twitter (welke regelmatig kunnen wijzigen) om te lezen wat zij met uw (persoons)gegevens doen die zij via deze cookies verwerken.
</p>
<p>
	De informatie die ze verzamelen wordt zo veel mogelijk geanonimiseerd. De informatie wordt overgebracht naar en door Twitter, Facebook, Google + en LinkedIn opgeslagen op servers in de Verenigde Staten. LinkedIn, Twitter, Facebook en Google + stellen zich te houden aan de Safe Harbor principes en zijn aangesloten bij het Safe Harbor-programma van het Amerikaanse Ministerie van Handel. Dit houdt in dat er sprake is van een passend beschermingsniveau voor de verwerking van eventuele persoonsgegevens.
</p>
<h3>3. In- en uitschakelen en verwijdering van cookies</h3>
<p>
	Wilt u cookies inschakelen of uitschakelen - of uw instellingen wijzigen? Dit kan via de instellingen van uw browser (bijvoorbeeld Internet Explorer, Safari, Firefox, Mozilla of Chrome). Meer informatie over het in- en uitschakelen en het verwijderen van cookies vindt u in de instructies en/of met behulp van de Help-functie van uw browser. Deze verschillen per browser. U kunt ervoor kiezen om in uw browser de 'do not track' functionaliteit activeren.
</p>
<h4><em>Verwijderen en accepteren tracking cookies en cookies geplaatst door derden:</em></h4>
<p>
	Sommige tracking cookies worden geplaatst door derden die onder meer via onze website advertenties aan u vertonen die aansluiten op uw voorkeuren. Deze cookies kunt u centraal verwijderen via
	<a href="http://www.youronlinechoices.eu" target="_blank">Your Online Choices</a> zodat ze niet bij een website van een derde teruggeplaatst worden.
</p>
<p>
	U kunt via de instellingen van uw browser (bijvoorbeeld Internet Explorer, Safari, Firefox, Mozilla of Chrome) aangeven of en, zo ja,
	<strong>welke cookies</strong> u accepteert. Het verschilt per browser waar deze instellingen zich bevinden. Via de 'help'-functie van uw browser kunt u de exacte locatie en werkwijze achterhalen.
</p>
<h4><em>Weigeren van specifieke cookies:</em></h4>
<p>
	Als u (bepaalde) cookies weigert, kan het zijn dat u niet of niet volledig gebruik kunt maken van de functionaliteiten van onze website.
</p>
<p>
	Het weigeren van cookies betekent overigens niet dat u geen advertenties meer te zien krijgt. U zult nog steeds advertenties zien in uw browser, maar deze zijn dan<strong> niet</strong> langer afgestemd op uw wensen en voorkeuren.
</p>
<h3>Informatie over cookies</h3>
<p>
	Op de volgende websites vindt u meer informatie over cookies:
</p>
<ul>
	<li>Consumentenbond: “<a href="http://www.consumentenbond.nl/test/elektronica-communicatie/veilig-online/privacy-op-internet/extra/wat-zijn-cookies/" target="_blank">Wat zijn cookies?</a>”
	</li>
	<li>Consumentenbond: “<a href="http://www.consumentenbond.nl/test/elektronica-communicatie/veilig-online/privacy-op-internet/extra/cookies-verwijderen/" target="_blank">Cookies verwijderen</a>”
	</li>
	<li>Consumentenbond: “<a href="http://www.consumentenbond.nl/test/elektronica-communicatie/veilig-online/privacy-op-internet/extra/cookies-uitschakelen/" target="_blank">Cookies uitschakelen</a>”
	</li>
</ul>
<p>
	Neem <a href="">contact</a> met ons op als u vragen heeft.
</p>