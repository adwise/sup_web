<?php

class adwiseImageOptim {

	/** @var \modX $modx */
	public $modx;

	/** @var \Adwise $adwise */
	public $adwise;

	protected $sHistoryFile = '.imageoptim';

	public function __construct(modX &$modx, Adwise &$adwise, array $configuration = array()) {
		$this->modx =& $modx;
		$this->adwise =& $adwise;
	}

	/**
	 * @param            $sFile
	 * @param bool|false $bIgnoreHistory
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function optimise($sFile, $bIgnoreHistory = false) {
		if ($bIgnoreHistory || !$this->_inHistory($sFile)) {
			$oOptim = new PHPImageOptim\PHPImageOptim();

			$aCommands = array();

			$ext = pathinfo($sFile, PATHINFO_EXTENSION);
			switch($ext) {
				case 'jpg':
				case 'jpeg':
					if ($sBin = $this->adwise->findBinary('jpegoptim'))
						$aCommands[] = $this->_getCommand('\PHPImageOptim\Tools\Jpeg\JpegOptim', $sBin);
					else
						$this->modx->log(MODX_LOG_LEVEL_INFO, 'unable to find jpegoptim binary');

					if ($sBin = $this->adwise->findBinary('jpegtran'))
						$aCommands[] = $this->_getCommand('\PHPImageOptim\Tools\Jpeg\JpegTran', $sBin);
					else
						$this->modx->log(MODX_LOG_LEVEL_INFO, 'unable to find jpegtran binary');

					break;
				case 'png':
					if ($sBin = $this->adwise->findBinary('pngquant'))
						$aCommands[] = $this->_getCommand('\PHPImageOptim\Tools\Png\PngQuant', $sBin);
					else
						$this->modx->log(MODX_LOG_LEVEL_INFO, 'unable to find pngquant binary');

					if ($sBin = $this->adwise->findBinary('optipng'))
						$aCommands[] = $this->_getCommand('\PHPImageOptim\Tools\Png\OptiPng', $sBin);
					else
						$this->modx->log(MODX_LOG_LEVEL_INFO, 'unable to find optipng binary');

					break;
				case 'gif':
					if ($sBin = $this->adwise->findBinary('gifsicle'))
						$aCommands[] = $this->_getCommand('\PHPImageOptim\Tools\Gif\Gifsicle', $sBin);
					else
						$this->modx->log(MODX_LOG_LEVEL_INFO, 'unable to find gifsicle binary');

					break;
				case 'svg':
					if ($sBin = $this->adwise->findBinary('svgo'))
						$aCommands[] = $this->_getCommand('\PHPImageOptim\Tools\Svg\Svgo', $sBin);
					else
						$this->modx->log(MODX_LOG_LEVEL_INFO, 'unable to find svgo binary');

					break;
			}

			if (!empty($aCommands)) {
				foreach($aCommands as $oCommand) {
					$oOptim->chainCommand($oCommand);
				}

				$oOptim->setImage($sFile);

				try {
					$oOptim->optimise();
				}
				catch(Exception $e) {
					return false;
				}

				$this->_writeHistory($sFile);

				return true;
			}
		}

		return null;
	}

	private function _inHistory($sFile) {
		$sPath = $this->adwise->option(Adwise::alias . '.path_static') . DIRECTORY_SEPARATOR;

		$aHistory = $this->_getHistory();

		return isset($aHistory[substr($sFile, strlen($sPath) - 1)]);
	}

	private function _getHistory() {
		$sPath = $this->adwise->option(Adwise::alias . '.path_static') . DIRECTORY_SEPARATOR;

		$sHistoryFile = $sPath . $this->sHistoryFile;
		$aHistory = file_exists($sHistoryFile) ? json_decode(file_get_contents($sHistoryFile), true) : array();

		return $aHistory;
	}

	private function _getCommand($sClass, $sBinary) {
		$oCommand = new $sClass();
		$oCommand->setBinaryPath($sBinary);

		return $oCommand;
	}

	private function _writeHistory($sFile) {
		$sPath = $this->adwise->option(Adwise::alias . '.path_static') . DIRECTORY_SEPARATOR;

		$aHistory = $this->_getHistory();
		$aHistory[substr($sFile, strlen($sPath) - 1)] = true;

		file_put_contents($sPath . $this->sHistoryFile, json_encode($aHistory));
	}
}
