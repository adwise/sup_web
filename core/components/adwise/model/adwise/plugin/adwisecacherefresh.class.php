<?php

class adwiseCacheRefresh extends adwisePlugin {

	public function onHandleRequest() {
		if (
			empty($this->modx->context) ||
			$this->modx->context->get('key') == 'mgr' ||
			(!$this->adwise->option(Adwise::alias . '.development:bool') && !$this->modx->user->get('sudo'))
		)
			return false;

		if (!isset($_SERVER['HTTP_CACHE_CONTROL']) || $_SERVER['HTTP_CACHE_CONTROL'] !== 'no-cache' || !isset($_SERVER['HTTP_PRAGMA']) || $_SERVER['HTTP_PRAGMA'] !== 'no-cache')
			return null;

		$this->modx->cacheManager->refresh(
			array(
				'scripts' => array(),
				'resource' => array('contexts' => array_diff(array($this->modx->context->get('key')), array('mgr')))
			)
		);
	}
}