<?php

class adwiseMediaSources extends adwisePlugin {

	public function onBeforeTempFormSave() {
		$this->_updateMediaSources();
	}

	private function _updateMediaSources() {
		$this->modx->log(Modx::LOG_LEVEL_INFO, '[Adwise Media Sources] Forcing basePath onto the Media Sources...');

		$sources = $this->modx->getIterator('modMediaSource');
		$paths = $this->adwise->option(Adwise::alias . '.media_sources:fromjson');

		foreach($sources as $source) {
			$path = $this->adwise->option($source->get('name'), $paths);
			if (empty($path))
				continue;

			$properties = $source->get('properties');

			if (isset($properties['basePath']) && is_array($properties['basePath'])) {
				$properties['basePath']['value'] = $path;
			}

			$source->set('properties', $properties);

			if (!$source->save()) {
				$this->modx->log(Modx::LOG_LEVEL_ERROR, '[Adwise Media Sources] Failed updating ' . $source->get('name') . '!');
			}
			else {
				$this->modx->log(Modx::LOG_LEVEL_INFO, '[Adwise Media Sources] Succesfuly updated ' . $source->get('name') . '.');
			}
		}
	}

	public function onCategorySave() {
		$this->_updateMediaSources();
	}

	public function onChunkSave() {
		$this->_updateMediaSources();
	}

	public function onPluginSave() {
		$this->_updateMediaSources();
	}

	public function onSiteRefresh() {
		$this->_updateMediaSources();
	}

	public function onSnippetSave() {
		$this->_updateMediaSources();
	}

	public function onTemplateSave() {
		$this->_updateMediaSources();
	}

	public function onTVFormSave() {
		$this->_updateMediaSources();
	}
}