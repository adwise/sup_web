<?php

class adwiseLexicons extends adwisePlugin {

	public function onHandleRequest() {
		$lexicons = $this->adwise->option(Adwise::alias . '.lexicons:csv');

		if (!empty($lexicons)) {
			foreach($lexicons as $lexicon) {
				$this->modx->lexicon->load($lexicon);
			}
		}
	}
}