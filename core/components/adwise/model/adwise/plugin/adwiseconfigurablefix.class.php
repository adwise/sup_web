<?php

class adwiseConfigurableFix extends adwisePlugin {

	var $aAfter = array(
		'adwiseDomainController'
	);

	public function onInitCulture() {
		$sContextKey = $this->modx->context->get('key');

		$aConfig = $this->adwise->config($sContextKey);
		foreach($aConfig as $sKey => $sValue) {
			$this->modx->config[$sKey] = $sValue;
		}
	}
}