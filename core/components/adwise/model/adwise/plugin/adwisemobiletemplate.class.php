<?php

class adwiseMobileTemplate extends adwisePlugin {

	public function onHandleRequest() {
		if ($this->modx->context->key == 'mgr')
			return;

		$bMobile = $this->_isMobile();
		if ($bMobile) {
			$this->modx->setOption('cache_ext', '.mobile.cache.php');
		}

        $this->modx->toPlaceholder('mobile',$bMobile ,Adwise::alias);
	}

	private function _isMobile($bCookie = true) {
		if ($bCookie && $_COOKIE['device_force'] == 'desktop')
			return false;
		else if ($bCookie && $_COOKIE['device_force'] == 'mobile')
			return true;

		$detect = new Mobile_Detect;

		return $detect->isMobile();
	}

	public function onLoadWebDocument() {
		if ($this->modx->context->key == 'mgr')
			return;

		if ($this->_isMobile()) {
			$oTemplate = $this->modx->getObject('modTemplate', array('id' => $this->modx->resource->get('template')));

			if (!empty($oTemplate)) {
				$aProperties = $oTemplate->getProperties();

				if ($this->adwise->option('template.mobile', $aProperties, false)) {
					$this->modx->resource->set('template', $this->adwise->option('template.mobile', $aProperties));
					$this->modx->resource->set('_content', null);
				}
			}
		}
	}
}
