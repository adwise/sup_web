<?php

class adwiseMinifyHtml extends adwisePlugin {

	public function onWebPagePrerender() {
		if (!$this->adwise->option(Adwise::alias . '.development:bool') && $this->adwise->option(Adwise::alias . '.optim_html:bool', null, false)) {
			$search = array(
				// strip comments
				'/<!--(.*)-->/Uis',
				// strip whitespaces after tags, except spaces
				'/\>[^\S ]+/s',
				// strip whitespaces before tags, except spaces
				'/[^\S ]+\</s',
				// shorten multiple whitespaces
				'/(\s)+/s'
			);

			$replace = array(
				'',
				'> ',
				'<',
				'\\1'
			);

			$this->modx->resource->_output = preg_replace($search, $replace, $this->modx->resource->_output);
		}
	}
}