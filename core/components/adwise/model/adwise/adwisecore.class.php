<?php

class AdwiseCore {

	/** @var \modX $modx */
	public $modx;

	/** @var \xPDOCacheManager $oCache */
	public $oCache;

	/** @var  array $aConfigs */
	protected $aConfigs;

	/** @var  array $aPlugins */
	protected $aPlugins;

	protected $aBinaryPaths = array(
		'/usr/local/bin/',
		'/usr/bin/'
	);

	public function __construct(modX &$modx, array $aConfiguration = array()) {
		$this->modx =& $modx;

		$vendorAutoload = dirname(MODX_CORE_PATH) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
		if (file_exists($vendorAutoload)) {
			require_once $vendorAutoload;
		}

		if ($this->option(Adwise::alias . '.binary_path', null, false)) {
			array_unshift($this->aBinaryPaths, $this->option(Adwise::alias . '.binary_path'));
		}

		if (!isset($_ENV['PATH']))
			$_ENV['PATH'] = '';

		foreach(array_reverse($this->aBinaryPaths) as $sPath) {
			if (!in_array($sPath, explode(':', $_ENV['PATH']))) {
				putenv('PATH=' . $_ENV['PATH'] . ':' . $sPath);
			}
		}
	}

	public function option($sKey, $aOptions = null, $mDefault = null, $bSkipEmpty = false) {
		$aMultiDim = explode('/', $sKey);
		if (sizeof($aMultiDim) > 1) {
			$mFirst = array_shift($aMultiDim);
			$aOptions = $this->option($mFirst, $aOptions);
			if (is_array($aOptions)) {
				$sKey = implode('/', $aMultiDim);

				return $this->option($sKey, $aOptions, $mDefault, $bSkipEmpty);
			}
			else
				return $mDefault;
		}

		$aParts = explode(':', $sKey);

		$sKey = array_shift($aParts);
		$aModifiers =& $aParts;

		if (is_numeric($sKey) && isset($aOptions[$sKey]))
			$mValue = $aOptions[$sKey];
		else
			$mValue = $this->modx->getOption($sKey, $aOptions, $mDefault, $bSkipEmpty);

		foreach($aModifiers as $sModifier) {
			switch($sModifier) {
				case 'bool':
					$mValue = (boolean)intval($mValue);
					break;

				case 'string':
					$mValue = (string)(is_bool($mValue) ? ($mValue ? 'true' : 'false') : $mValue);
					break;

				case 'alias':
					/** @var \modResource $resource */
					$resource = !empty($this->modx->resource) ? $this->modx->resource : $this->modx->newObject('modResource');

					if (empty($resource))
						return false;

					$mValue = $resource->cleanAlias($mValue);
					break;

				case 'float':
					$mValue = floatval($mValue);
					break;

				case 'int':
					$mValue = intval($mValue);
					break;

				case 'double':
					$mValue = (double)$mValue;
					break;

				case 'abs':
					$mValue = abs($mValue);
					break;

				case 'array':
					$mValue = (array)$mValue;
					break;

				case 'trim':
					if (is_scalar($mValue))
						$mValue = trim($mValue);
					else if (is_array($mValue)) {
						foreach($mValue as $sKey => &$row) {
							$row = trim($row);
						}
						unset($row);
					}
					break;

				case 'fromjson':
					$mValue = $this->modx->fromJSON($mValue);
					break;

				case 'tojson':
					$mValue = $this->modx->toJSON($mValue);
					break;

				case 'count':
					$mValue = (is_array($mValue) || is_object($mValue)) ? count($mValue) : strlen($mValue);
					break;

				case 'empty':
					$mValue = empty($mValue);
					break;

				case 'csv':
					$mValue = is_string($mValue) ? explode(',', $mValue) : (is_array($mValue) ? join(',', $mValue) : $mValue);
					break;

				case 'ssv':
					$mValue = is_string($mValue) ? explode(';', $mValue) : (is_array($mValue) ? join(';', $mValue) : $mValue);
					break;

				case 'slash':
					$mValue = is_string($mValue) ? explode('/', $mValue) : (is_array($mValue) ? join('/', $mValue) : $mValue);
					break;

				case 'tolower':
					$mValue = strtolower($mValue);
					break;

				case 'toupper':
					$mValue = strtoupper($mValue);
					break;

				case 'ucfirst':
					$mValue = ucfirst($mValue);
					break;

				case 'lcfirst':
					$mValue = lcfirst($mValue);
					break;

				case 'urldecode':
					$mValue = urldecode($mValue);
					break;

				case 'urlencode':
					$mValue = urlencode($mValue);
					break;

				case 'md5':
					$mValue = md5($mValue);
					break;

				case 'sha1':
					$mValue = sha1($mValue);
					break;

				case 'totime':
					$mValue = strtotime($mValue);
					break;

				case 'htmlentities':
					$mValue = htmlentities($mValue, ENT_QUOTES, 'UTF-8');
					break;

				case 'path':
					$mValue = strtr(realpath($mValue), '\\', '/');
					break;
			}
		}

		return $mValue;
	}

	public function plugin(modSystemEvent &$oEvent, array $aProperties = array()) {
		$this->_initPlugins();

		foreach($this->aPlugins as $oPlugin) {
			if (method_exists($oPlugin, $oEvent->name)) {
				call_user_func_array(
					array(
						$oPlugin,
						$oEvent->name
					),
					array(
						&$oEvent,
						$aProperties
					)
				);
			}
		}
	}

	private function _initPlugins() {
		if (!$this->aPlugins) {
			require_once(dirname(__FILE__) . '/adwiseplugin.class.php');

			$this->aPlugins = array();

			$sPluginsDirectory = 'plugin';

			$oReflection = new ReflectionClass($this);
			$sPluginsPath = dirname($oReflection->getFilename()) . DIRECTORY_SEPARATOR . $sPluginsDirectory;

			if (file_exists($sPluginsPath)) {
				$oHandle = opendir($sPluginsPath);
				while(($sFile = readdir($oHandle)) !== false) {
					if (substr($sFile, -10) == '.class.php') {
						$sClass = substr($sFile, 0, -10);
						$this->aPlugins[] = $this->initClass($sPluginsDirectory . '.' . $sClass);
					}
				}
			}

			usort(
				$this->aPlugins,
				function (AdwisePlugin &$oA, AdwisePlugin &$oB) {
					return $oA->compare($oB);
				}
			);
		}
	}

	public function initClass($sClass, array $aConfiguration = array()) {
		$oReflection = new ReflectionClass($this);
		$sClass = $this->modx->loadClass($sClass, dirname($oReflection->getFilename()) . '/', false, true);

		if (empty($sClass)) {
			$this->modx->log(MODX_LOG_LEVEL_ERROR, 'Could not load class: ' . $sClass);

			return false;
		}

		$oInstance = new $sClass($this->modx, $this, $aConfiguration);
		if (!($oInstance instanceof $sClass))
			return false;

		return $oInstance;
	}

	public function config($sContextKey = null, $bReload = false) {
		if (!isset($this->aConfigs[$sContextKey]) || $bReload) {
			$sCacheKey = 'config/' . $sContextKey . '/config';

			$aConfig = null;

			if (isset($this->oCache) && !$bReload) {
				$sCachePath = (isset($this->modx->config) && isset($this->modx->config['cache_path'])) ? $this->modx->config['cache_path'] : null;
				if ($sCachePath) {
					$iConfigTime = filemtime($sCachePath . 'adw/' . $sCacheKey . '.cache.php');
					$iSystemTime = filemtime($sCachePath . 'system_settings/config.cache.php');
					$iContextTime = filemtime($sCachePath . 'context_settings/' . $sContextKey . '/context.cache.php');

					if ($iConfigTime && $iSystemTime && $iContextTime && $iConfigTime > $iSystemTime && $iConfigTime > $iContextTime)
						$aConfig = $this->oCache->get($sCacheKey);
				}
			}

			if (empty($aConfig)) {
				global $config_options;

				$aConfig = array_merge(
					array(
						'base_url' => MODX_BASE_URL,
						'base_path' => MODX_BASE_PATH,
						'core_path' => MODX_CORE_PATH,
						'url_scheme' => MODX_URL_SCHEME,
						'http_host' => MODX_HTTP_HOST,
						'site_url' => MODX_SITE_URL,
						'manager_path' => MODX_MANAGER_PATH,
						'manager_url' => MODX_MANAGER_URL,
						'assets_path' => MODX_ASSETS_PATH,
						'assets_url' => MODX_ASSETS_URL,
						'connectors_path' => MODX_CONNECTORS_PATH,
						'connectors_url' => MODX_CONNECTORS_URL,
						'connector_url' => MODX_CONNECTORS_URL . 'index.php',
						'processors_path' => MODX_PROCESSORS_PATH,
						'request_param_id' => 'id',
						'request_param_alias' => 'q',
						'https_port' => isset($GLOBALS['https_port']) ? $GLOBALS['https_port'] : 443,
						'error_handler_class' => 'error.modErrorHandler',
						'server_port' => isset($_SERVER['SERVER_PORT']) ? $_SERVER['SERVER_PORT'] : ''
					),
					$config_options
				);

				$aSystemSettings = $this->modx->getIterator('modSystemSetting');
				foreach($aSystemSettings as $oSetting) {
					$aConfig[$oSetting->get('key')] = $oSetting->get('value');
				}

				if (!empty($sContextKey)) {
					$aConfigSettings = $this->modx->getIterator(
						'modContextSetting',
						array(
							'context_key' => $sContextKey,
							'value:!=' => ''
						)
					);

					foreach($aConfigSettings as $oSetting) {
						$aConfig[$oSetting->get('key')] = $oSetting->get('value');
					}
				}

				$bChanges = true;
				while($bChanges) {
					$bChanges = false;

					foreach($aConfig as &$sValue) {
						if ($this->parseConfigurable($aConfig, $sValue))
							$bChanges = true;
					}
				}
				unset($sValue);

				if (isset($this->oCache) && $this->oCache != '')
					$this->oCache->set($sCacheKey, $aConfig);
			}

			$this->aConfigs[$sContextKey] = $aConfig;
		}

		return $this->aConfigs[$sContextKey];
	}

	public function parseConfigurable(&$aConfig, &$sValue) {
		if (!is_string($sValue))
			return false;

		$bChanges = false;

		if (preg_match_all('/\{(?!")(.*?)(?!")\}/', $sValue, $aMatches, PREG_SET_ORDER)) {
			foreach($aMatches as $aReplacement) {
				if (isset($aConfig[$aReplacement[1]])) {
					$sNewValue = str_replace($aReplacement[0], $aConfig[$aReplacement[1]], $sValue);
					if ($sNewValue != $sValue) {
						$sValue = $sNewValue;
						$bChanges = true;
					}
				}
			}
		}

		return $bChanges;
	}

	public function findBinary($sBinary) {
		foreach($this->aBinaryPaths as $sPath) {
			exec('/usr/bin/which ' . $sPath . $sBinary . ' 2>&1', $aOutput, $iResult);
			if ($iResult === 0)
				return $sPath . $sBinary;
		}

		return false;
	}

	public function getResourceByTVValue($sTvName, $sValue) {
		$query = $this->modx->newQuery('modResource');
		$query->innerJoin('modTemplateVarResource', 'TemplateVarResources');
		$query->innerJoin('modTemplateVar', 'TemplateVar', 'TemplateVarResources.tmplvarid = TemplateVar.id');
		$query->where(
			array(
				'TemplateVar.name' => $sTvName,
				'TemplateVarResources.value' => $sValue
			)
		);
		$query->limit(1);

		return $this->modx->getObject('modResource', $query);
	}
}
