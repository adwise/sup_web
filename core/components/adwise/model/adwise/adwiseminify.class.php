<?php

class adwiseMinify {

	/** @var \modX $modx */
	public $modx;

	/** @var \Adwise $adwise */
	public $adwise;

	public function __construct(modX &$modx, Adwise &$adwise, array $configuration = array()) {
		$this->modx =& $modx;
		$this->adwise =& $adwise;
	}

	public function minify($aUrls, $sExtension = 'js', $bCombine = true, $bCompress = true) {
		if (!$bCombine) {
			$aFiles = array();
			foreach($aUrls as $sUrl) {
				$aFiles[] = $this->_minify(array($sUrl), $sExtension, $bCompress);
			}

			return $aFiles;
		}

		return array($this->_minify($aUrls, $sExtension, $bCompress));
	}

	private function _minify($aUrls, $sExtension = 'js', $bCompress = true) {
		$sHash = md5(implode(';', array_keys($aUrls)));
		$sCacheFile = $this->_getCachePath($sExtension) . $sHash . '.' . $sExtension;

		if (!file_exists($sCacheFile) || $this->_checkFilemtime($sCacheFile, $aUrls)) {
			$sOutput = '';

			foreach($aUrls as $sUrl) {
				$sData = trim($this->_getFileContent($sUrl));

				if ($sExtension == 'css') {
					$sData = $this->_prepareCSSFile($sData, $sUrl, true);
				}

				$sOutput .= '/* ' . $sUrl . " */\n\n" . $sData . "\n\n";
			}

			if ($bCompress)
				$sOutput = str_replace('/*!', '/*', $sOutput);

			file_put_contents($sCacheFile, $sOutput);

			if ($bCompress) {
				$sCompressed = $this->_getCompressed($sCacheFile, $sExtension);
				if ($sCompressed)
					file_put_contents($sCacheFile, $sCompressed);
			}
		}

		return $this->_getCacheUrl($sExtension) . $sHash . '.' . $sExtension;
	}

	public function _getCachePath($sExtension) {
		return $this->adwise->option(Adwise::alias . '.path_static') . 'shared/' . ($sExtension == 'css' ? 'style' : 'script') . '/min/';
	}

	protected function _checkFilemtime($sCachefile, array $aUrls) {
		$iReference = filemtime($sCachefile);

		foreach($aUrls as $sUrl) {
			$sFile = $this->_getUrlPath($sUrl);
			if ($sFile && file_exists($sFile) && filemtime($sFile) > $iReference)
				return true;
		}

		return false;
	}

	protected function _getUrlPath($sUrl) {
		if (strpos($sUrl, $this->adwise->option(Adwise::alias . '.url_static')) === 0)
			return str_replace($this->adwise->option(Adwise::alias . '.url_static'), $this->adwise->option(Adwise::alias . '.path_static'), $sUrl);

		return false;
	}

	protected function _getFileContent($sUrl) {
		$sPath = $this->_getUrlPath($sUrl);
		if ($sPath) {
			if (file_exists($sPath))
				return file_get_contents($sPath);
			else
				return '';
		}

		$oCurl = curl_init();
		curl_setopt_array(
			$oCurl,
			array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_CONNECTTIMEOUT => 5,
				CURLOPT_TIMEOUT => 5,
				CURLOPT_URL => $sUrl
			)
		);

		return curl_exec($oCurl);
	}

	protected function _prepareCSSFile($sData, $sUrl) {
		$aUrlParts = explode('/', $sUrl);
		array_pop($aUrlParts);
		$sBaseUrl = implode('/', $aUrlParts) . '/';

		$sData = $this->_replaceCSSImport($sData, $sBaseUrl);
		$sData = $this->_replaceCSSUrl($sData, $sBaseUrl);

		return $sData;
	}

	protected function _replaceCSSImport($sCss, $sBaseUrl) {
		preg_match_all("/@import (url\(\"?)?(url\()?(\")?(.*?)(?(1)\")+(?(2)\))+(?(3)\")/", $sCss, $aMatches);

		foreach($aMatches[4] as $i => $sFile) {
			if (strpos(strtolower($sFile), 'http://') === false && strpos(strtolower($sFile), 'https://') === false) {
				if (strpos(strtolower($sFile), '//') === 0) {
					$sFile = (strpos(strtolower($sBaseUrl), 'https://') === false ? 'http:' : 'https:') . $sFile;
				}
				else {
					$sFile = $sBaseUrl . $sFile;
				}

				$sData = trim($this->_getFileContent($sFile));
				$sData = $this->_prepareCSSFile($sData, $sFile);

				$sCss = str_replace($aMatches[0][$i], $sData, $sCss);
			}
		}

		return $sCss;
	}

	protected function _replaceCSSUrl($sCss, $sBaseUrl) {
		return preg_replace('#url\((?!\s*([\'"]?((((?:https?:)?//))|((?:data\:?:)))))\s*([\'"])?#', 'url($6' . $sBaseUrl, $sCss);
	}

	protected function _getCompressed($sFile, $sType = 'js') {
		$sBinary = $this->adwise->findBinary('yuicompressor');
		if ($sBinary) {
			exec($sBinary . ' --type ' . (strtolower($sType) == 'css' ? 'css' : 'js') . ' ' . $sFile . ' 2>&1', $aOutput, $iResult);
			if ($iResult != 0) {
				$this->modx->log(MODX_LOG_LEVEL_ERROR, 'yuicompressor was unable to optimise file, result: ' . $iResult . ' file: ' . $sFile);

				return false;
			}
		}
		else
			$this->modx->log(MODX_LOG_LEVEL_INFO, 'unable to find yuicompressor binary');

		return !empty($aOutput) ? implode("\n", $aOutput) : false;
	}

	protected function _getCacheUrl($sExtension) {
		return $this->adwise->option(Adwise::alias . '.url_static') . 'shared/' . ($sExtension == 'css' ? 'style' : 'script') . '/min/';
	}

}