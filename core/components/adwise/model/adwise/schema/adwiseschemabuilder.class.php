<?php

class adwiseSchemaBuilder {

	/** @var \modX $modx */
	public $modx;

	/** @var \Adwise $adwise */
	public $adwise;

	/** @var string $sModelPath */
	protected $sModelPath = null;

	/** @var string $smodelName */
	protected $sModelName = null;

	public function __construct(modX &$modx, Adwise &$adwise) {
		$this->modx =& $modx;
		$this->adwise =& $adwise;
	}

	public function init($sModelPath, $sModelName) {
		$this->sModelPath = $sModelPath;
		$this->sModelName = $sModelName;

		return $this;
	}

	public function build($bRemove = false, $bDropTables = false) {
		$this->modx->loadClass('transport.modPackageBuilder', '', false, true);
		$this->modx->setLogLevel(modX::LOG_LEVEL_INFO);
		$this->modx->setLogTarget('ECHO');

		$aSources = array(
			'model' => $this->sModelPath,
			'schema_file' => $this->sModelPath . 'schema/mysql.schema.xml'
		);

		$oManager = $this->modx->getManager();
		$oGenerator = $oManager->getGenerator();

		if (!is_dir($aSources['model'])) {
			$this->modx->log(modX::LOG_LEVEL_ERROR, 'Model directory not found!');

			return false;
		}
		if (!file_exists($aSources['schema_file'])) {
			$this->modx->log(modX::LOG_LEVEL_ERROR, 'Schema file not found!');

			return false;
		}

		$aMap = $this->_getMap();
		foreach($aMap as $sClass => $aData) {
			if ($bDropTables) {
				$this->modx->exec('DROP TABLE `' . $this->adwise->option('table_prefix') . $aData['table'] . '`');
			}
			if ($bRemove) {
				if (file_exists($this->sModelPath . $this->sModelName . '/mysql/' . strtolower($sClass) . '.class.php'))
					unlink($this->sModelPath . $this->sModelName . '/mysql/' . strtolower($sClass) . '.class.php');

				if (file_exists($this->sModelPath . $this->sModelName . '/mysql/' . strtolower($sClass) . '.map.inc.php'))
					unlink($this->sModelPath . $this->sModelName . '/mysql/' . strtolower($sClass) . '.map.inc.php');
			}
		}

		$oGenerator->parseSchema($aSources['schema_file'], $aSources['model']);
		$this->modx->addPackage($this->sModelName, $aSources['model']);

		$aMap = $this->_getMap();
		foreach($aMap as $sClass => $aData) {
			$oManager->createObjectContainer($sClass);
		}

		return true;
	}

	protected function _getMap() {
		$aMap = array();
		if (file_exists($this->sModelPath . $this->sModelName . '/metadata.mysql.php')) {
			$xpdo_meta_map = array();
			include($this->sModelPath . $this->sModelName . '/metadata.mysql.php');

			foreach($xpdo_meta_map as $sType => $aClasses) {
				foreach($aClasses as $sClass) {
					$aMap[$sClass] = $this->getClassMap($sClass);
				}
			}

		}

		return $aMap;
	}

	protected function getClassMap($sClass) {
		$xpdo_meta_map = array();
		if (file_exists($this->sModelPath . $this->sModelName . '/mysql/' . strtolower($sClass) . '.map.inc.php'))
			include($this->sModelPath . $this->sModelName . '/mysql/' . strtolower($sClass) . '.map.inc.php');

		return $xpdo_meta_map[$sClass];
	}
}