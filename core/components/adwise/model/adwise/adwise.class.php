<?php
require_once(dirname(__FILE__) . '/adwisecore.class.php');

class Adwise extends AdwiseCore {

	const alias = 'adw';

	public function __construct(modX &$modx, array $aConfiguration = array()) {
		parent::__construct($modx, $aConfiguration);

		$this->modx->adwise =& $this;

		$this->_initialize();
	}

	private function _initialize() {
		$this->oCache = $this->initClass('cache.adwiseCacheManager');
	}
}