<?php

class adwiseCacheManager extends xPDOCacheManager {

	/** @var \Adwise $adwise */
	public $adwise;

	public function __construct(modX &$modx, Adwise &$adwise, array $configuration = array()) {
		parent::__construct($modx, $configuration);

		$this->adwise =& $adwise;
	}

	final public function set($key, &$value, $lifetime = 0, $configuration = array()) {
		$c =& $configuration;

		return parent::set(
			$key,
			$value,
			$lifetime,
			array(
				'cache_path' => $this->adwise->option('cache_path', $c),
				xPDO::OPT_CACHE_KEY => isset($c[xPDO::OPT_CACHE_KEY]) ? $c[xPDO::OPT_CACHE_KEY] : Adwise::alias
			)
		);
	}

	final public function get($key, $configuration = array()) {
		$c =& $configuration;

		return parent::get(
			$key,
			array(
				'cache_path' => $this->adwise->option('cache_path', $c),
				xPDO::OPT_CACHE_KEY => isset($c[xPDO::OPT_CACHE_KEY]) ? $c[xPDO::OPT_CACHE_KEY] : Adwise::alias
			)
		);
	}

	final public function delete($key, $configuration = array()) {
		$c =& $configuration;

		return parent::delete(
			$key,
			array(
				'cache_path' => $this->adwise->option('cache_path', $c),
				xPDO::OPT_CACHE_KEY => isset($c[xPDO::OPT_CACHE_KEY]) ? $c[xPDO::OPT_CACHE_KEY] : Adwise::alias
			)
		);
	}
}