<?php

/* Contact form */
$_lang['Send'] = 'Versturen';

/* Mixed Contact form / Email report */
$_lang['Firstname'] = 'Voornaam';
$_lang['Lastname'] = 'Achternaam';
$_lang['Email'] = 'E-mailadres';
$_lang['Telephone']  = 'Telefoonnummer';
$_lang['Text'] = 'Bericht';

/* Email report */
$_lang['email_header-text'] = 'Op [[!+adw.now:date=`%d-%m-%Y`]] om [[!+adw.now:date=`%H:%M`]] heeft u een contactverzoek naar ons via onze <a href="[[++site_name]]" target="_blank" style="color: #08512a; text-decoration: none;">website</a> verzonden. Het verzoek is als volgt:';
$_lang['email_header-header'] = 'Aanvraag van pagina "[[*pagetitle]]"';

