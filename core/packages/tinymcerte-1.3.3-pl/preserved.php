<?php return array (
  '842bb10f4f30583468852764b56a6fa3' => 
  array (
    'criteria' => 
    array (
      'name' => 'tinymcerte',
    ),
    'object' => 
    array (
      'name' => 'tinymcerte',
      'path' => '{core_path}components/tinymcerte/',
      'assets_path' => '{assets_path}components/tinymcerte/',
    ),
  ),
  'f1225efa2ec1a6971444370ca2d54d82' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.plugins',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.plugins',
      'value' => 'advlist autolink lists modximage charmap print preview anchor visualblocks searchreplace code fullscreen insertdatetime media table contextmenu paste modxlink',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '2016-02-18 13:54:25',
    ),
  ),
  '23494b408b59b0a85d8bbe5a49e1df2c' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.toolbar1',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.toolbar1',
      'value' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.toolbar',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'cc92f89597f21b1b0fbb318a55fecd47' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.toolbar2',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.toolbar2',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.toolbar',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '705302f48976cd547d258ef5a8b4563c' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.toolbar3',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.toolbar3',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.toolbar',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '5e6b6e3ca5b666cef3b42be592f87237' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.menubar',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.menubar',
      'value' => 'file edit insert view format table tools',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '2eb2e2abbbf5e4f4d669b63a645d4481' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.statusbar',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.statusbar',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'c3cf4f4d386c3756d4ad35c3c3814e04' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.image_advtab',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.image_advtab',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '9c7b77a6de2354d7a98b0c307bb5b643' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.object_resizing',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.object_resizing',
      'value' => '1',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'de81edc3195a32457a396d0fd81a7e36' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.style_formats',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.style_formats',
      'value' => '[{"title": "Headers", "items": "headers_format"},{"title": "Inline", "items": "inline_format"},{"title": "Blocks", "items": "blocks_format"},{"title": "Alignment", "items": "alignment_format"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'f33dd1ac2cc2ce2627d1f6ba5aa712f2' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.headers_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.headers_format',
      'value' => '[{"title": "Header 1", "format": "h1"},{"title": "Header 2", "format": "h2"},{"title": "Header 3", "format": "h3"},{"title": "Header 4", "format": "h4"},{"title": "Header 5", "format": "h5"},{"title": "Header 6", "format": "h6"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '5ac0a67a2e777c606855b417b4c1b71d' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.inline_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.inline_format',
      'value' => '[{"title": "Bold", "icon": "bold", "format": "bold"},{"title": "Italic", "icon": "italic", "format": "italic"},{"title": "Underline", "icon": "underline", "format": "underline"},{"title": "Strikethrough", "icon": "strikethrough", "format": "strikethrough"},{"title": "Superscript", "icon": "superscript", "format": "superscript"},{"title": "Subscript", "icon": "subscript", "format": "subscript"},{"title": "Code", "icon": "code", "format": "code"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '843a3e7357ef95556de7227e18cf13eb' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.blocks_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.blocks_format',
      'value' => '[{"title": "Paragraph", "format": "p"},{"title": "Blockquote", "format": "blockquote"},{"title": "Div", "format": "div"},{"title": "Pre", "format": "pre"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '6aaf6dd5f77902ff79b7a105e4a4016d' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.alignment_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.alignment_format',
      'value' => '[{"title": "Left", "icon": "alignleft", "format": "alignleft"},{"title": "Center", "icon": "aligncenter", "format": "aligncenter"},{"title": "Right", "icon": "alignright", "format": "alignright"},{"title": "Justify", "icon": "alignjustify", "format": "alignjustify"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '6f0aa066881fa53b10d12a5f1cb8950d' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.paste_as_text',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.paste_as_text',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '6183f1f5c21369ab76657cd2e914e3cf' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.style_formats_merge',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.style_formats_merge',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'c794e576231013f61ee6520f801dae62' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.link_class_list',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.link_class_list',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '53e27866079642f583820cb7bd5a03c7' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.browser_spellcheck',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.browser_spellcheck',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '81b48deaf392b6d631aa7220a6197665' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.content_css',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.content_css',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '61bb470f9f4e7660415f6a33f50af97b' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.image_class_list',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.image_class_list',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'c7965b67d8ba67c2b3fa1d34769a95ec' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.external_config',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.external_config',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '87107d6d8ca3697f0b162621133e261c' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.skin',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.skin',
      'value' => 'modx',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '1a8863de1b026e71f3da40ac2b733a32' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.relative_urls',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.relative_urls',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '157edb6f1dd3ea5098923198cf973c99' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.remove_script_host',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.remove_script_host',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'bd70a3cee73bef913bdbb660c3a06e3f' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.valid_elements',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.valid_elements',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'd47440a7663945dee4b5003cd109d0a6' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.links_across_contexts',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.links_across_contexts',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'ee153b22c006800dfdb3557137715d84' => 
  array (
    'criteria' => 
    array (
      'category' => 'TinyMCE Rich Text Editor',
    ),
    'object' => 
    array (
      'id' => 23,
      'parent' => 0,
      'category' => 'TinyMCE Rich Text Editor',
      'rank' => 0,
    ),
  ),
  'eb35f9222f523a7bf3c69a7419e4f8e0' => 
  array (
    'criteria' => 
    array (
      'name' => 'TinyMCERTE',
    ),
    'object' => 
    array (
      'id' => 10,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'TinyMCERTE',
      'description' => '',
      'editor_type' => 0,
      'category' => 23,
      'cache_type' => 0,
      'plugincode' => '/**
 * TinyMCE Rich Tech Editor
 *
 */
$corePath = $modx->getOption(\'tinymcerte.core_path\', null, $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/tinymcerte/\');
/** @var TinyMCERTE $tinymcerte */
$tinymcerte = $modx->getService(
    \'tinymcerte\',
    \'TinyMCERTE\',
    $corePath . \'model/tinymcerte/\',
    array(
        \'core_path\' => $corePath
    )
);

$className = \'TinyMCERTE\' . $modx->event->name;
$modx->loadClass(\'TinyMCERTEPlugin\', $tinymcerte->getOption(\'modelPath\') . \'tinymcerte/events/\', true, true);
$modx->loadClass($className, $tinymcerte->getOption(\'modelPath\') . \'tinymcerte/events/\', true, true);
if (class_exists($className)) {
    /** @var TinyMCERTEPlugin $handler */
    $handler = new $className($modx, $scriptProperties);
    $handler->run();
}
return;',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * TinyMCE Rich Tech Editor
 *
 */
$corePath = $modx->getOption(\'tinymcerte.core_path\', null, $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/tinymcerte/\');
/** @var TinyMCERTE $tinymcerte */
$tinymcerte = $modx->getService(
    \'tinymcerte\',
    \'TinyMCERTE\',
    $corePath . \'model/tinymcerte/\',
    array(
        \'core_path\' => $corePath
    )
);

$className = \'TinyMCERTE\' . $modx->event->name;
$modx->loadClass(\'TinyMCERTEPlugin\', $tinymcerte->getOption(\'modelPath\') . \'tinymcerte/events/\', true, true);
$modx->loadClass($className, $tinymcerte->getOption(\'modelPath\') . \'tinymcerte/events/\', true, true);
if (class_exists($className)) {
    /** @var TinyMCERTEPlugin $handler */
    $handler = new $className($modx, $scriptProperties);
    $handler->run();
}
return;',
    ),
  ),
  '2c8f0a5d9c5181b9dd4ff67d8a83de12' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextBrowserInit',
    ),
    'object' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextBrowserInit',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '0584b2c8fff24bf2277cf09158b039ee' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorRegister',
    ),
    'object' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorRegister',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '47b3996fade98b7c00c0eb8f3d2041a2' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorInit',
    ),
    'object' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorInit',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);