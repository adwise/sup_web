<?php return array (
  '54d4e569382837194455bfd73860f1f9' => 
  array (
    'criteria' => 
    array (
      'name' => 'superboxselect',
    ),
    'object' => 
    array (
      'name' => 'superboxselect',
      'path' => '{core_path}components/superboxselect/',
      'assets_path' => '{assets_path}components/superboxselect/',
    ),
  ),
  '153566558ca8b26b44b8d589dea7013a' => 
  array (
    'criteria' => 
    array (
      'key' => 'superboxselect.debug',
    ),
    'object' => 
    array (
      'key' => 'superboxselect.debug',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'superboxselect',
      'area' => 'system',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'c048fd9cecf9e9b5e364754fc4838c22' => 
  array (
    'criteria' => 
    array (
      'key' => 'superboxselect.advanced',
    ),
    'object' => 
    array (
      'key' => 'superboxselect.advanced',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'superboxselect',
      'area' => 'system',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '54471c82659e83ed8695c160fe427de9' => 
  array (
    'criteria' => 
    array (
      'category' => 'SuperBoxSelect',
    ),
    'object' => 
    array (
      'id' => 47,
      'parent' => 0,
      'category' => 'SuperBoxSelect',
      'rank' => 0,
    ),
  ),
  '57d566d16431bc44bf0d4c7f42c7270e' => 
  array (
    'criteria' => 
    array (
      'name' => 'SuperBoxSelect',
    ),
    'object' => 
    array (
      'id' => 19,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'SuperBoxSelect',
      'description' => 'SuperBoxSelect runtime hooks - registers custom TV input types and includes javascripts on document edit pages.',
      'editor_type' => 0,
      'category' => 47,
      'cache_type' => 0,
      'plugincode' => '/**
 * SuperBoxSelect Runtime Hooks
 *
 * Registers custom TV input types and includes javascripts on
 * document edit pages so that the TV can be used from within other extras
 * (i.e. MIGX, Collections)
 *
 * @package superboxselect
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVInputPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$corePath = $modx->getOption(\'superboxselect.core_path\', null, $modx->getOption(\'core_path\') . \'components/superboxselect/\');
/** @var SuperBoxSelect $superboxselect */
$superboxselect = $modx->getService(\'superboxselect\', \'SuperBoxSelect\', $corePath . \'model/superboxselect/\', array(
    \'core_path\' => $corePath
));

switch ($modx->event->name) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'superboxselect:default\');
        $tvId = isset($modx->controller->scriptProperties[\'id\']) ? $modx->controller->scriptProperties[\'id\'] : 0;
        /** @var modTemplateVar $tv */
        $tv = $modx->getObject(\'modTemplateVar\', $tvId);
        if ($tv) {
            $tvProperties = $tv->get(\'input_properties\');
            $package = isset($tvProperties[\'selectPackage\']) ? $tvProperties[\'selectPackage\'] : \'\';
        } else {
            $package = \'\';
        }
        $superboxselect->includeScriptAssets($package);
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnDocFormRender\':
        $superboxselect->includeScriptAssets();
        break;
};',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'disabled' => 1,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * SuperBoxSelect Runtime Hooks
 *
 * Registers custom TV input types and includes javascripts on
 * document edit pages so that the TV can be used from within other extras
 * (i.e. MIGX, Collections)
 *
 * @package superboxselect
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVInputPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$corePath = $modx->getOption(\'superboxselect.core_path\', null, $modx->getOption(\'core_path\') . \'components/superboxselect/\');
/** @var SuperBoxSelect $superboxselect */
$superboxselect = $modx->getService(\'superboxselect\', \'SuperBoxSelect\', $corePath . \'model/superboxselect/\', array(
    \'core_path\' => $corePath
));

switch ($modx->event->name) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'superboxselect:default\');
        $tvId = isset($modx->controller->scriptProperties[\'id\']) ? $modx->controller->scriptProperties[\'id\'] : 0;
        /** @var modTemplateVar $tv */
        $tv = $modx->getObject(\'modTemplateVar\', $tvId);
        if ($tv) {
            $tvProperties = $tv->get(\'input_properties\');
            $package = isset($tvProperties[\'selectPackage\']) ? $tvProperties[\'selectPackage\'] : \'\';
        } else {
            $package = \'\';
        }
        $superboxselect->includeScriptAssets($package);
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnDocFormRender\':
        $superboxselect->includeScriptAssets();
        break;
};',
    ),
  ),
  '507c88a1a51bf450af2e65b384e3034f' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnManagerPageBeforeRender',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnManagerPageBeforeRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'dd46074eecc54edbc7a83841b6bd5c88' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputPropertiesList',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputPropertiesList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '046a4523b6b85e532632adde71bc0c38' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputRenderList',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputRenderList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'bac2d8c24c15a9ba072cc9819d63c856' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnDocFormRender',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnDocFormRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);