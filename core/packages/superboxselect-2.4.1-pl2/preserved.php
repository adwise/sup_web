<?php return array (
  'ec0ebe3bd68ac8fb0e12f5474e8beb86' => 
  array (
    'criteria' => 
    array (
      'name' => 'superboxselect',
    ),
    'object' => 
    array (
      'name' => 'superboxselect',
      'path' => '{core_path}components/superboxselect/',
      'assets_path' => '{assets_path}components/superboxselect/',
    ),
  ),
  '41d6d5658ba1b50c2083a54905f2fbd9' => 
  array (
    'criteria' => 
    array (
      'key' => 'superboxselect.debug',
    ),
    'object' => 
    array (
      'key' => 'superboxselect.debug',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'superboxselect',
      'area' => 'system',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '19c59a911567039a467288c821869392' => 
  array (
    'criteria' => 
    array (
      'key' => 'superboxselect.advanced',
    ),
    'object' => 
    array (
      'key' => 'superboxselect.advanced',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'superboxselect',
      'area' => 'system',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'f2460b3d4492aa3d28b0cb2b3d03178c' => 
  array (
    'criteria' => 
    array (
      'category' => 'SuperBoxSelect',
    ),
    'object' => 
    array (
      'id' => 47,
      'parent' => 0,
      'category' => 'SuperBoxSelect',
      'rank' => 0,
    ),
  ),
  'e244abb12db986bea09e5165917626ec' => 
  array (
    'criteria' => 
    array (
      'name' => 'SuperBoxSelect',
    ),
    'object' => 
    array (
      'id' => 19,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'SuperBoxSelect',
      'description' => 'SuperBoxSelect runtime hooks - registers custom TV input types and includes javascripts on document edit pages.',
      'editor_type' => 0,
      'category' => 47,
      'cache_type' => 0,
      'plugincode' => '/**
 * SuperBoxSelect Runtime Hooks
 *
 * Registers custom TV input types and includes javascripts on
 * document edit pages so that the TV can be used from within other extras
 * (i.e. MIGX, Collections)
 *
 * @package superboxselect
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVInputPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$corePath = $modx->getOption(\'superboxselect.core_path\', null, $modx->getOption(\'core_path\') . \'components/superboxselect/\');
/** @var SuperBoxSelect $superboxselect */
$superboxselect = $modx->getService(\'superboxselect\', \'SuperBoxSelect\', $corePath . \'model/superboxselect/\', array(
    \'core_path\' => $corePath
));

switch ($modx->event->name) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'superboxselect:default\');
        $tvId = isset($modx->controller->scriptProperties[\'id\']) ? $modx->controller->scriptProperties[\'id\'] : 0;
        /** @var modTemplateVar $tv */
        $tv = $modx->getObject(\'modTemplateVar\', $tvId);
        if ($tv) {
            $tvProperties = $tv->get(\'input_properties\');
            $package = isset($tvProperties[\'selectPackage\']) ? $tvProperties[\'selectPackage\'] : \'\';
        } else {
            $package = \'\';
        }
        $superboxselect->includeScriptAssets($package);
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnDocFormRender\':
        $superboxselect->includeScriptAssets();
        break;
};',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * SuperBoxSelect Runtime Hooks
 *
 * Registers custom TV input types and includes javascripts on
 * document edit pages so that the TV can be used from within other extras
 * (i.e. MIGX, Collections)
 *
 * @package superboxselect
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVInputPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$corePath = $modx->getOption(\'superboxselect.core_path\', null, $modx->getOption(\'core_path\') . \'components/superboxselect/\');
/** @var SuperBoxSelect $superboxselect */
$superboxselect = $modx->getService(\'superboxselect\', \'SuperBoxSelect\', $corePath . \'model/superboxselect/\', array(
    \'core_path\' => $corePath
));

switch ($modx->event->name) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'superboxselect:default\');
        $tvId = isset($modx->controller->scriptProperties[\'id\']) ? $modx->controller->scriptProperties[\'id\'] : 0;
        /** @var modTemplateVar $tv */
        $tv = $modx->getObject(\'modTemplateVar\', $tvId);
        if ($tv) {
            $tvProperties = $tv->get(\'input_properties\');
            $package = isset($tvProperties[\'selectPackage\']) ? $tvProperties[\'selectPackage\'] : \'\';
        } else {
            $package = \'\';
        }
        $superboxselect->includeScriptAssets($package);
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnDocFormRender\':
        $superboxselect->includeScriptAssets();
        break;
};',
    ),
  ),
  'e09857987cba8b67c9cd7b5014e067a9' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnManagerPageBeforeRender',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnManagerPageBeforeRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '2e28e5e08bd002db84977892e207b81b' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputPropertiesList',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputPropertiesList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '8f098df5a72ef75e17e18ab5492e5e17' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputRenderList',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputRenderList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '1af5aa4ab462d2ce9ac81732e55e80cb' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnDocFormRender',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnDocFormRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);