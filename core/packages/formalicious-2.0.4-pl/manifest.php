<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'license' => 'Formalicious is proprietary software, developed by Sterc and distributed through modmore.com. By purchasing Formalicious via https://www.modmore.com/formalicious/, you have received a usage license for a single (1) MODX Revolution installation, including one year (starting on date of purchase) of email support.

While we hope Formalicious is useful to you and we will try to help you successfully use Formalicious, modmore or Sterc is not liable for loss of revenue, data, damages or other financial loss resulting from the installation or use of Formalicious.

By using and installing this package, you acknowledge that you shall only use this on a single MODX installation.

Redistribution in any shape or form is strictly prohibited. You may customize or change the provided source code to tailor Formalicious for your own use, as long as no attempt is made to remove license protection measures. By changing source code you acknowledge you void the right to support unless coordinated with modmore support.
',
    'readme' => '--------------------
Formalicious
--------------------
Author: Sterc <modx@sterc.nl>
--------------------

Formalicious is the most powerful and easiest MODX form builder, with built-in multi-step forms, 8 field types, validation and the ability to use hooks and other advanced FormIt features.

Important! Since Formalicious 2.0.0 we refactored some snippets and chunks. The snippet RenderForm is replaced with FormaliciousRenderForm.
Please check your snippet and the script properties and check if the chunks/templates of the \'Fieldtypes\' are correct..
',
    'changelog' => 'Version 2.0.4-pl
- Fixed undefined bug when clicking View in FormIt in overview grid

Version 2.0.3-pl
- Set required integers by default

Version 2.0.2-pl
- Set currentUrl with step=1 when steps are there
- Make the id column on fields visible

Version 2.0.1-pl
--------------------------
- Add sterc extra settings when not existing

Version 2.0.0-pl
--------------------------
- Add form DB fields published_from and published_till to auto (de)publication forms
- Add field answer DB field selected to set default selected
- Rich text support for field description and form email content.
- Steps (navigation) above the form
- New parameter stepRedirect to redirect a step to a non resource ID (if stepRedirect is set to \'request\' the step will be redirected to the current REQUEST URL)
- New permissions added
    - formalicious_admin to show/hide admin panel
    - formalicious_tab_fields to show/hide fields tab
    - formalicious_tab_advanced to show/hide advanced tab (formalicious_advanced renamed to formalicious_tab_advanced)
- ExtJS refactored for faster and better UI/UX
    - Step preview fixed
    - Toggleable description, placeholder, required and heading fields for each fieldtype
- RenderForm replaced with FormaliciousRenderForm
- All snippets and chunks are prefixed with Formalicious

Version 1.4.1-pl
--------------------------
- Create database fields on update

Version 1.4.0-pl
--------------------------
- Add field description
- Hide advanced tab based on permissions
- Add heading & description fields
- Add field description
- Change fiarcontent from varchar to text for bigger mails

Version 1.3.1-pl
--------------------------
- Add system setting for disable form saving on install
- Change fiarcontent from varchar to text

Version 1.3.0-pl
--------------------------
- Fixed phptype of some fields in schema of tables (PHP 7 compatibility)
- Added system setting to disable overall form saving functionality
- Added russian lexicon

Version 1.2.1-pl (October 2017)
--------------------------
- Remove the limit from the ContentBlocks input field
- Hide autoreply options when autoreply checkbox is unchecked

Version 1.2.0-pl (August 2nd, 2017)
--------------------------
- Removing default limit from fiaremailto field (#31)
- Add back button to form update view
- Add duplicate option to forms grid (#32)
- Update grid action buttons to use modx font-awesome icons
- Make add step/field buttons more visible
- Add preview option to form fields tab
- Add saveTmpFiles FormIt property to default formTpl
- Add formName FormIt property to default formTpl
- Prefix fiar-attachment field with modx base_path
- Only add email hook when emailto is not empty
- Remove default limit of 20 from field-values grid
- Check for common \'spam,email,redirect\' hooks added by Formalicious when saving posthooks
- Add ID field to form-fields grid
- Make sure prehooks are run before the renderForm snippet

Version 1.1.0-pl (April 19th, 2017)
--------------------------
- Fix setting placeholder for stepParam parameter for renderForm
- Show message when trying to display unpublished form (#6)
- Update radio and checkbox chunks to use correct Bootstrap classes (#28)
- Allow emailTpl and fiarTpl to be overwritten with renderForm snippet parameters (#23)
- Add validate and customValidators parameters to renderForm and formTpl (#23)

Version 1.0.1-pl (February 3rd, 2017)
--------------------------
- Added ContentBlocks support (thanks Mark!)
- Fixed installation issues with MODX installations with custom table-prefixes

Version 1.0.0-pl (February 1st, 2017)
--------------------------
- XS-4 New documentation
- XS-11 Changed height of several dialog windows
- XS-12 Spacing adjustments
- XS-19 Gave the default emails a lighter grey
- XS-20 Modified all en/nl lexicons
- XS-21 Fixed inline editing (removed it)

Version 1.0.0-RC2 (January 27th, 2017)
--------------------------
- [#28] Fixed oldstyle actions
- [#29] Improved this very changelog
- [#40] Create a readme
- [#41] New logo for the modmore site!
- [#XS-42] Autoheight for new-field dialog

Version 1.0.0-RC1 (January 26th, 2017)
--------------------------
- [#34] Improved handling of empty fields
- [#37] Radio button # Select # Checkbox options are now required
- [#38] Allowed files are now mentioned
- [#36] Improved default emails
- [#32] Unused description field is now removed
- [#31] Improved placeholder field usage
- [#30] Mention context-NAME in the "Redirect to" field when creating a new form
- [#27] Fixed file upload in multistep form
- [#22] Improved emailTpl
- [#20 + #23 + #35] Improved styling of buttons
- [#17] Fixed category_id fallback
- [#9 + #12] Fixed empty fields in multistep form
- [#13] Fixed email validation
- [#10] Fixed adding parameters not working properly
- [#7] Now shipped with TV
- [#8] Fixed uninstallation proces
- [#4] "Update type" dialog is now bigger
- [#2] Fixed select form-email-field when creating a form
- [#1] Fixed empty field when creating a form
- [#6] Improved adding fields
- [#5] Improved step-creation flow
- [#3] Replaced form-description with "Email header text"

Version 0.6.0 (2016)
--------------------------
- Create form categories
- Ability to create form steps
- Ability to save forms in FormIt (FormIt V2.2.2#) CMP
- Added ability to setup autoresponder in form
- Updated lexicons
',
    'setup-options' => 'formalicious-2.0.4-pl/setup-options.php',
    'requires' => 
    array (
      'formit' => '>=4.1.1',
    ),
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => 'cf44f56c404a217f83de85f9d07689d0',
      'native_key' => 'formalicious',
      'filename' => 'modNamespace/459a640c474f54d577e3888f2f0bc9e3.vehicle',
      'namespace' => 'formalicious',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '06cd99c55ad0be060c5188f142d5f163',
      'native_key' => 'formalicious.branding_url',
      'filename' => 'modSystemSetting/8e727215b562892e8c1ed250eefbfb10.vehicle',
      'namespace' => 'formalicious',
    ),
    2 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '2900fd650d34f9218fc20c80d2110a74',
      'native_key' => 'formalicious.branding_url_help',
      'filename' => 'modSystemSetting/b79ea39a8f95583e30a06bd325b5ece3.vehicle',
      'namespace' => 'formalicious',
    ),
    3 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'fb928f75ece5c7989c313c74ab8a5af9',
      'native_key' => 'formalicious.saveforms',
      'filename' => 'modSystemSetting/0f299915aacaf39d8a059d1630a20e13.vehicle',
      'namespace' => 'formalicious',
    ),
    4 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'faec46492822f5254716281abc84a4ca',
      'native_key' => 'formalicious.saveforms_prefix',
      'filename' => 'modSystemSetting/00d920a8eedd252e8756bfcc55b057b3.vehicle',
      'namespace' => 'formalicious',
    ),
    5 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '5cf1f5e8052fb6225fb3628908cd648d',
      'native_key' => 'formalicious.disallowed_hooks',
      'filename' => 'modSystemSetting/b0ab6b091748edb9c6563be859d52336.vehicle',
      'namespace' => 'formalicious',
    ),
    6 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '5959ed5f3030a751b8dd506c41d0e4c9',
      'native_key' => 'formalicious.preview_css',
      'filename' => 'modSystemSetting/3968dd63fa97422cfeb2967999e79a34.vehicle',
      'namespace' => 'formalicious',
    ),
    7 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'dbf81fad0ab915a1cdddfecc0a6d5c67',
      'native_key' => 'formalicious.source',
      'filename' => 'modSystemSetting/0c934dc18f7f86d88efb2d8178204675.vehicle',
      'namespace' => 'formalicious',
    ),
    8 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd95ce2682d473c7525bccf01caeeecc8',
      'native_key' => 'formalicious.use_editor',
      'filename' => 'modSystemSetting/d0f7e0c8816fd5c581f055d3471adcd4.vehicle',
      'namespace' => 'formalicious',
    ),
    9 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'f94317a230499b4f8aad320728fb2e1d',
      'native_key' => 'formalicious.editor_menubar',
      'filename' => 'modSystemSetting/0cd85adc3202ece594e6708a2ded5676.vehicle',
      'namespace' => 'formalicious',
    ),
    10 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '59cd1216f07478b57288c5381961f7b9',
      'native_key' => 'formalicious.editor_plugins',
      'filename' => 'modSystemSetting/89c2392c2e3738866a853cd17dab4c54.vehicle',
      'namespace' => 'formalicious',
    ),
    11 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '7eb572bcbffee647d5004ce8ecfcf4ab',
      'native_key' => 'formalicious.editor_statusbar',
      'filename' => 'modSystemSetting/139a0824e03b25b06dfdb992bcfa3ef9.vehicle',
      'namespace' => 'formalicious',
    ),
    12 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '793928777868285e49cc3805e2599c91',
      'native_key' => 'formalicious.editor_toolbar1',
      'filename' => 'modSystemSetting/908af5f092ad1f0b8ffc55b0279f991b.vehicle',
      'namespace' => 'formalicious',
    ),
    13 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'c9d3302b5c8b906d54b1590059f92d8a',
      'native_key' => 'formalicious.editor_toolbar2',
      'filename' => 'modSystemSetting/350e6a93a6410d506140fcebef7e718e.vehicle',
      'namespace' => 'formalicious',
    ),
    14 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '2704ef3641b71f82a29662d0431b56af',
      'native_key' => 'formalicious.editor_toolbar3',
      'filename' => 'modSystemSetting/81d7d93254e3b35f5e4971fed37bbc4f.vehicle',
      'namespace' => 'formalicious',
    ),
    15 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => '1d8b4b2da91b69d1badec0f0407801b4',
      'native_key' => NULL,
      'filename' => 'modCategory/e7e002cc5a0d2ae9fc858171e8fd91d5.vehicle',
      'namespace' => 'formalicious',
    ),
    16 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modMenu',
      'guid' => '58e20b0b9c950057da7584c94d1d39d5',
      'native_key' => 'formalicious',
      'filename' => 'modMenu/01e710ed07fa7e2e2f6d05a990b3a6d2.vehicle',
      'namespace' => 'formalicious',
    ),
  ),
);