<?php return array (
  'cf44f56c404a217f83de85f9d07689d0' => 
  array (
    'criteria' => 
    array (
      'name' => 'formalicious',
    ),
    'object' => 
    array (
      'name' => 'formalicious',
      'path' => '{core_path}components/formalicious/',
      'assets_path' => '{assets_path}components/formalicious/',
    ),
  ),
  '06cd99c55ad0be060c5188f142d5f163' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.branding_url',
    ),
    'object' => 
    array (
      'key' => 'formalicious.branding_url',
      'value' => 'https://www.sterc.nl/modx/extras/formalicious',
      'xtype' => 'textfield',
      'namespace' => 'formalicious',
      'area' => 'formalicious',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '2900fd650d34f9218fc20c80d2110a74' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.branding_url_help',
    ),
    'object' => 
    array (
      'key' => 'formalicious.branding_url_help',
      'value' => 'https://docs.modmore.com/en/Formalicious/v1/index.html',
      'xtype' => 'textfield',
      'namespace' => 'formalicious',
      'area' => 'formalicious',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'fb928f75ece5c7989c313c74ab8a5af9' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.saveforms',
    ),
    'object' => 
    array (
      'key' => 'formalicious.saveforms',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'formalicious',
      'area' => 'formalicious',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'faec46492822f5254716281abc84a4ca' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.saveforms_prefix',
    ),
    'object' => 
    array (
      'key' => 'formalicious.saveforms_prefix',
      'value' => 'Formalicious',
      'xtype' => 'textfield',
      'namespace' => 'formalicious',
      'area' => 'formalicious',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '5cf1f5e8052fb6225fb3628908cd648d' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.disallowed_hooks',
    ),
    'object' => 
    array (
      'key' => 'formalicious.disallowed_hooks',
      'value' => 'spam,email,redirect,FormaliciousHookHandleForm,FormaliciousHookRemoveForm',
      'xtype' => 'textfield',
      'namespace' => 'formalicious',
      'area' => 'formalicious',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '5959ed5f3030a751b8dd506c41d0e4c9' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.preview_css',
    ),
    'object' => 
    array (
      'key' => 'formalicious.preview_css',
      'value' => '/assets/components/formalicious/css/mgr/preview.css',
      'xtype' => 'textfield',
      'namespace' => 'formalicious',
      'area' => 'formalicious',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'dbf81fad0ab915a1cdddfecc0a6d5c67' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.source',
    ),
    'object' => 
    array (
      'key' => 'formalicious.source',
      'value' => '1',
      'xtype' => 'textfield',
      'namespace' => 'formalicious',
      'area' => 'formalicious',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'd95ce2682d473c7525bccf01caeeecc8' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.use_editor',
    ),
    'object' => 
    array (
      'key' => 'formalicious.use_editor',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'formalicious',
      'area' => 'formalicious_editor',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'f94317a230499b4f8aad320728fb2e1d' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.editor_menubar',
    ),
    'object' => 
    array (
      'key' => 'formalicious.editor_menubar',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'formalicious',
      'area' => 'formalicious_editor',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '59cd1216f07478b57288c5381961f7b9' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.editor_plugins',
    ),
    'object' => 
    array (
      'key' => 'formalicious.editor_plugins',
      'value' => 'advlist autolink lists modximage charmap print preview anchor visualblocks searchreplace code fullscreen insertdatetime media table contextmenu paste modxlink',
      'xtype' => 'textfield',
      'namespace' => 'formalicious',
      'area' => 'formalicious_editor',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '7eb572bcbffee647d5004ce8ecfcf4ab' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.editor_statusbar',
    ),
    'object' => 
    array (
      'key' => 'formalicious.editor_statusbar',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'formalicious',
      'area' => 'formalicious_editor',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '793928777868285e49cc3805e2599c91' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.editor_toolbar1',
    ),
    'object' => 
    array (
      'key' => 'formalicious.editor_toolbar1',
      'value' => 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | link',
      'xtype' => 'textfield',
      'namespace' => 'formalicious',
      'area' => 'formalicious_editor',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'c9d3302b5c8b906d54b1590059f92d8a' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.editor_toolbar2',
    ),
    'object' => 
    array (
      'key' => 'formalicious.editor_toolbar2',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'formalicious',
      'area' => 'formalicious_editor',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '2704ef3641b71f82a29662d0431b56af' => 
  array (
    'criteria' => 
    array (
      'key' => 'formalicious.editor_toolbar3',
    ),
    'object' => 
    array (
      'key' => 'formalicious.editor_toolbar3',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'formalicious',
      'area' => 'formalicious_editor',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '1d8b4b2da91b69d1badec0f0407801b4' => 
  array (
    'criteria' => 
    array (
      'category' => 'Formalicious',
    ),
    'object' => 
    array (
      'id' => 48,
      'parent' => 0,
      'category' => 'Formalicious',
      'rank' => 0,
    ),
  ),
  'db532f96e7471a37939372e31f800a32' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousEmailTpl',
    ),
    'object' => 
    array (
      'id' => 56,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousEmailTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl">
<head>
    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0">    <!-- So that mobile webkit will display zoomed in -->
    <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
    <title>[[++site_name]]</title>

    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            width: 100% !important;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        p {
            margin-top: 2px;
            margin-bottom: 10px;
        }

        table {
            border-spacing: 0;
        }

        table td {
            border-collapse: collapse;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        .ReadMsgBody {
            width: 100%;
            padding: 0;
            margin: 0;
            background-color: #f5f5f5;
        }

        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        @media screen and (max-width: 599px) {
            h2[class=full],
            table[class=force-row],
            table[class=container],
            table[class=article-image-full] {
                width: 100% !important;
                max-width: 100% !important;
            }
        }

        @media screen and (max-width: 599px) {
            tr.visible-lg,
            tr[class=visible=lg],
            table.visible-lg,
            table[class=visible-lg] {
                display: none !important;
            }
        }

        @media screen and (max-width: 599px) {
            .logo {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
        }

        @media screen and (max-width: 599px) {
            .cta {
                height: auto !important;
            }
        }

        @media screen and (max-width: 599px) {
            .header-line {
                padding-left: 10px;
                padding-right: 10px;
                display: block;
            }
        }

        @media screen and (max-width: 599px) {
            .articleleft-image {
                margin-bottom: 20px;
            }
        }

        @media screen and (max-width: 599px) {
            .articleright-image {
                margin-top: 20px;
            }
        }

        @media screen and (max-width: 400px) {
            td[class*=container-padding] {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }

        .ios-footer a {
            color: #aaaaaa !important;
            text-decoration: underline;
        }

        @media screen and (max-width: 599px) {
            td[class=col] {
                width: 100% !important;
            }
        }

        @media screen and (max-width: 599px) {
            td[class=small-center] {
                text-align: center !important;
            }
        }

        @media screen and (max-width: 599px) {
            table[class=visible-lg] {
                display: none !important;
            }
        }

        @media screen and (max-width: 599px) {
            tr[class=visible-xs] {
                display: block !important;
                width: auto !important;
                overflow: visible !important;
                float: none !important;
                height: auto !important;
            }
        }

        @media screen and (max-width: 599px) {
            td[class=cols-wrapper],
            td[class=2-cols-wrapper] {
            padding-top: 18px;
        }
        }

        @media screen and (max-width: 400px) {
            td[class=2-cols-wrapper] {
            padding-left: 12px !important;
            padding-right: 12px !important;
        }
        }

        @media screen and (max-width: 400px) {
            td[class=cols-wrapper] {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
        }

        @media screen and (max-width: 400px) {
            td[class=content-wrapper] {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }

        @media screen and (max-width: 400px) {
            table[class=spacer] {
                display: none !important;
            }
        }

        @media screen and (max-width: 400px) {
            table[class=article-image-full] {
                height: 100px !important;
            }
        }

        @media screen and (max-width: 400px) {
            .article-image-full {
                height: 100px !important;
            }
        }
    </style>
    <!--[if mso]>

    <style>
        h2, h4 {
            font-family: Arial, sans-serif !important;
        }
    </style>

    <![endif]-->
</head>
<body style="margin:0; padding:0;" bgcolor="#f5f5f5" marginwidth="0" marginheight="0" margintop="0" marginleft="0" marginright="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#f5f5f5">
    <tr>
        <td align="center" valign="top" bgcolor="#f5f5f5" style="background-color: #f5f5f5;">

            <!-- row to add space above the container -->
            <table width="100%" height="40" border="0" cellpadding="0" cellspacing="0" class="spacer" style="width: 600px;border:none;" bgcolor="#f5f5f5">
                <tr>
                    <td height="40" width="100%" bgcolor="#f5f5f5" style="background-color: #f5f5f5;">
                        <br>
                    </td>
                </tr>
            </table>
            <!-- /row to add space above the container -->

            <!-- 600px container (white background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;">
                <tr>
                    <td class="content" align="left" style="background-color:#ffffff;border:none;">

                        <!-- 600px container (white background) -->
                        <table width="600" border="0" cellpadding="0" cellspacing="0" class="force-row" style="width: 600px;border:none;">
                            <tr>
                                <td align="center" valign="top" bgcolor="#ffffff">
                                    <br>
                                    <br>
                                    <table border="0" width="600" cellpadding="20" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;padding: 20px;">
                                        <tr style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;background-color:#ffffff;">
                                            <td style="text-align:left;max-width: 100%;" width="560" class="force-row">
                                                <h2>[[+name]]</h2>
                                                [[+emailContent]]
                                            </td>
                                        </tr>
                                        <tr style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;background-color:#ffffff;">
                                            <td style="text-align:left;max-width: 100%;" width="560" class="force-row">
                                                [[+emailFields]]
                                            </td>
                                        </tr>
                                    </table>
                                    <br>&nbsp;
                                </td>
                            </tr>
                        </table>
                        <!-- /600px container (white background) -->
                    </td>
                </tr>
            </table>
            <!--/600px container -->

            <!-- 600px container (no background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;">
                <tr>
                    <td class="content" align="left" style="background-color:#f5f5f5;border:none;">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" class="force-row" style="width: 600px;border:none;">
                            <tr>
                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #5d6561; text-align: left; padding-top: 15px;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- 600px container (no background) -->
            <br>
        </td>
    </tr>
</table>
<!--/100% background wrapper-->
</body>
</html>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl">
<head>
    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0">    <!-- So that mobile webkit will display zoomed in -->
    <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
    <title>[[++site_name]]</title>

    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            width: 100% !important;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        p {
            margin-top: 2px;
            margin-bottom: 10px;
        }

        table {
            border-spacing: 0;
        }

        table td {
            border-collapse: collapse;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        .ReadMsgBody {
            width: 100%;
            padding: 0;
            margin: 0;
            background-color: #f5f5f5;
        }

        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        @media screen and (max-width: 599px) {
            h2[class=full],
            table[class=force-row],
            table[class=container],
            table[class=article-image-full] {
                width: 100% !important;
                max-width: 100% !important;
            }
        }

        @media screen and (max-width: 599px) {
            tr.visible-lg,
            tr[class=visible=lg],
            table.visible-lg,
            table[class=visible-lg] {
                display: none !important;
            }
        }

        @media screen and (max-width: 599px) {
            .logo {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
        }

        @media screen and (max-width: 599px) {
            .cta {
                height: auto !important;
            }
        }

        @media screen and (max-width: 599px) {
            .header-line {
                padding-left: 10px;
                padding-right: 10px;
                display: block;
            }
        }

        @media screen and (max-width: 599px) {
            .articleleft-image {
                margin-bottom: 20px;
            }
        }

        @media screen and (max-width: 599px) {
            .articleright-image {
                margin-top: 20px;
            }
        }

        @media screen and (max-width: 400px) {
            td[class*=container-padding] {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }

        .ios-footer a {
            color: #aaaaaa !important;
            text-decoration: underline;
        }

        @media screen and (max-width: 599px) {
            td[class=col] {
                width: 100% !important;
            }
        }

        @media screen and (max-width: 599px) {
            td[class=small-center] {
                text-align: center !important;
            }
        }

        @media screen and (max-width: 599px) {
            table[class=visible-lg] {
                display: none !important;
            }
        }

        @media screen and (max-width: 599px) {
            tr[class=visible-xs] {
                display: block !important;
                width: auto !important;
                overflow: visible !important;
                float: none !important;
                height: auto !important;
            }
        }

        @media screen and (max-width: 599px) {
            td[class=cols-wrapper],
            td[class=2-cols-wrapper] {
            padding-top: 18px;
        }
        }

        @media screen and (max-width: 400px) {
            td[class=2-cols-wrapper] {
            padding-left: 12px !important;
            padding-right: 12px !important;
        }
        }

        @media screen and (max-width: 400px) {
            td[class=cols-wrapper] {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
        }

        @media screen and (max-width: 400px) {
            td[class=content-wrapper] {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }

        @media screen and (max-width: 400px) {
            table[class=spacer] {
                display: none !important;
            }
        }

        @media screen and (max-width: 400px) {
            table[class=article-image-full] {
                height: 100px !important;
            }
        }

        @media screen and (max-width: 400px) {
            .article-image-full {
                height: 100px !important;
            }
        }
    </style>
    <!--[if mso]>

    <style>
        h2, h4 {
            font-family: Arial, sans-serif !important;
        }
    </style>

    <![endif]-->
</head>
<body style="margin:0; padding:0;" bgcolor="#f5f5f5" marginwidth="0" marginheight="0" margintop="0" marginleft="0" marginright="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#f5f5f5">
    <tr>
        <td align="center" valign="top" bgcolor="#f5f5f5" style="background-color: #f5f5f5;">

            <!-- row to add space above the container -->
            <table width="100%" height="40" border="0" cellpadding="0" cellspacing="0" class="spacer" style="width: 600px;border:none;" bgcolor="#f5f5f5">
                <tr>
                    <td height="40" width="100%" bgcolor="#f5f5f5" style="background-color: #f5f5f5;">
                        <br>
                    </td>
                </tr>
            </table>
            <!-- /row to add space above the container -->

            <!-- 600px container (white background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;">
                <tr>
                    <td class="content" align="left" style="background-color:#ffffff;border:none;">

                        <!-- 600px container (white background) -->
                        <table width="600" border="0" cellpadding="0" cellspacing="0" class="force-row" style="width: 600px;border:none;">
                            <tr>
                                <td align="center" valign="top" bgcolor="#ffffff">
                                    <br>
                                    <br>
                                    <table border="0" width="600" cellpadding="20" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;padding: 20px;">
                                        <tr style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;background-color:#ffffff;">
                                            <td style="text-align:left;max-width: 100%;" width="560" class="force-row">
                                                <h2>[[+name]]</h2>
                                                [[+emailContent]]
                                            </td>
                                        </tr>
                                        <tr style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;background-color:#ffffff;">
                                            <td style="text-align:left;max-width: 100%;" width="560" class="force-row">
                                                [[+emailFields]]
                                            </td>
                                        </tr>
                                    </table>
                                    <br>&nbsp;
                                </td>
                            </tr>
                        </table>
                        <!-- /600px container (white background) -->
                    </td>
                </tr>
            </table>
            <!--/600px container -->

            <!-- 600px container (no background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;">
                <tr>
                    <td class="content" align="left" style="background-color:#f5f5f5;border:none;">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" class="force-row" style="width: 600px;border:none;">
                            <tr>
                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #5d6561; text-align: left; padding-top: 15px;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- 600px container (no background) -->
            <br>
        </td>
    </tr>
</table>
<!--/100% background wrapper-->
</body>
</html>',
    ),
  ),
  '5c4da591bcb97efe5d3e6cceb32e17ca' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFiarEmailTpl',
    ),
    'object' => 
    array (
      'id' => 57,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFiarEmailTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl">
<head>
    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0">    <!-- So that mobile webkit will display zoomed in -->
    <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
    <title>[[++site_name]]</title>

    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            width: 100% !important;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        p {
            margin-top: 2px;
            margin-bottom: 10px;
        }

        table {
            border-spacing: 0;
        }

        table td {
            border-collapse: collapse;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        .ReadMsgBody {
            width: 100%;
            padding: 0;
            margin: 0;
            background-color: #f5f5f5;
        }

        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        @media screen and (max-width: 599px) {
            h2[class=full],
            table[class=force-row],
            table[class=container],
            table[class=article-image-full] {
                width: 100% !important;
                max-width: 100% !important;
            }
        }

        @media screen and (max-width: 599px) {
            tr.visible-lg,
            tr[class=visible=lg],
            table.visible-lg,
            table[class=visible-lg] {
                display: none !important;
            }
        }

        @media screen and (max-width: 599px) {
            .logo {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
        }

        @media screen and (max-width: 599px) {
            .cta {
                height: auto !important;
            }
        }

        @media screen and (max-width: 599px) {
            .header-line {
                padding-left: 10px;
                padding-right: 10px;
                display: block;
            }
        }

        @media screen and (max-width: 599px) {
            .articleleft-image {
                margin-bottom: 20px;
            }
        }

        @media screen and (max-width: 599px) {
            .articleright-image {
                margin-top: 20px;
            }
        }

        @media screen and (max-width: 400px) {
            td[class*=container-padding] {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }

        .ios-footer a {
            color: #aaaaaa !important;
            text-decoration: underline;
        }

        @media screen and (max-width: 599px) {
            td[class=col] {
                width: 100% !important;
            }
        }

        @media screen and (max-width: 599px) {
            td[class=small-center] {
                text-align: center !important;
            }
        }

        @media screen and (max-width: 599px) {
            table[class=visible-lg] {
                display: none !important;
            }
        }

        @media screen and (max-width: 599px) {
            tr[class=visible-xs] {
                display: block !important;
                width: auto !important;
                overflow: visible !important;
                float: none !important;
                height: auto !important;
            }
        }

        @media screen and (max-width: 599px) {
            td[class=cols-wrapper],
            td[class=2-cols-wrapper] {
            padding-top: 18px;
        }
        }

        @media screen and (max-width: 400px) {
            td[class=2-cols-wrapper] {
            padding-left: 12px !important;
            padding-right: 12px !important;
        }
        }

        @media screen and (max-width: 400px) {
            td[class=cols-wrapper] {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
        }

        @media screen and (max-width: 400px) {
            td[class=content-wrapper] {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }

        @media screen and (max-width: 400px) {
            table[class=spacer] {
                display: none !important;
            }
        }

        @media screen and (max-width: 400px) {
            table[class=article-image-full] {
                height: 100px !important;
            }
        }

        @media screen and (max-width: 400px) {
            .article-image-full {
                height: 100px !important;
            }
        }
    </style>
    <!--[if mso]>

    <style>
        h2, h4 {
            font-family: Arial, sans-serif !important;
        }
    </style>

    <![endif]-->
</head>
<body style="margin:0; padding:0;" bgcolor="#f5f5f5" marginwidth="0" marginheight="0" margintop="0" marginleft="0" marginright="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#f5f5f5">
    <tr>
        <td align="center" valign="top" bgcolor="#f5f5f5" style="background-color: #f5f5f5;">

            <!-- row to add space above the container -->
            <table width="100%" height="40" border="0" cellpadding="0" cellspacing="0" class="spacer" style="width: 600px;border:none;" bgcolor="#f5f5f5">
                <tr>
                    <td height="40" width="100%" bgcolor="#f5f5f5" style="background-color: #f5f5f5;">
                        <br>
                    </td>
                </tr>
            </table>
            <!-- /row to add space above the container -->

            <!-- 600px container (white background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;">
                <tr>
                    <td class="content" align="left" style="background-color:#ffffff;border:none;">

                        <!-- 600px container (white background) -->
                        <table width="600" border="0" cellpadding="0" cellspacing="0" class="force-row" style="width: 600px;border:none;">
                            <tr>
                                <td align="center" valign="top" bgcolor="#ffffff">
                                    <br>
                                    <br>
                                    <table border="0" width="600" cellpadding="20" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;padding: 20px;">
                                        <tr style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;background-color:#ffffff;">
                                            <td style="text-align:left;max-width: 100%;" width="560" class="force-row">
                                                <h2>[[+name]]</h2>
                                                [[+fiarContent]]
                                            </td>
                                        </tr>
                                        <tr style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;background-color:#ffffff;">
                                            <td style="text-align:left;max-width: 100%;" width="560" class="force-row">
                                                [[+fiarFields]]
                                            </td>
                                        </tr>
                                    </table>
                                    <br>&nbsp;
                                </td>
                            </tr>
                        </table>
                        <!-- /600px container (white background) -->
                    </td>
                </tr>
            </table>
            <!--/600px container -->

            <!-- 600px container (no background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;">
                <tr>
                    <td class="content" align="left" style="background-color:#f5f5f5;border:none;">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" class="force-row" style="width: 600px;border:none;">
                            <tr>
                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #5d6561; text-align: left; padding-top: 15px;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- 600px container (no background) -->
            <br>
        </td>
    </tr>
</table>
<!--/100% background wrapper-->
</body>
</html>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl">
<head>
    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0">    <!-- So that mobile webkit will display zoomed in -->
    <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
    <title>[[++site_name]]</title>

    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            width: 100% !important;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        p {
            margin-top: 2px;
            margin-bottom: 10px;
        }

        table {
            border-spacing: 0;
        }

        table td {
            border-collapse: collapse;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        .ReadMsgBody {
            width: 100%;
            padding: 0;
            margin: 0;
            background-color: #f5f5f5;
        }

        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        @media screen and (max-width: 599px) {
            h2[class=full],
            table[class=force-row],
            table[class=container],
            table[class=article-image-full] {
                width: 100% !important;
                max-width: 100% !important;
            }
        }

        @media screen and (max-width: 599px) {
            tr.visible-lg,
            tr[class=visible=lg],
            table.visible-lg,
            table[class=visible-lg] {
                display: none !important;
            }
        }

        @media screen and (max-width: 599px) {
            .logo {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
        }

        @media screen and (max-width: 599px) {
            .cta {
                height: auto !important;
            }
        }

        @media screen and (max-width: 599px) {
            .header-line {
                padding-left: 10px;
                padding-right: 10px;
                display: block;
            }
        }

        @media screen and (max-width: 599px) {
            .articleleft-image {
                margin-bottom: 20px;
            }
        }

        @media screen and (max-width: 599px) {
            .articleright-image {
                margin-top: 20px;
            }
        }

        @media screen and (max-width: 400px) {
            td[class*=container-padding] {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }

        .ios-footer a {
            color: #aaaaaa !important;
            text-decoration: underline;
        }

        @media screen and (max-width: 599px) {
            td[class=col] {
                width: 100% !important;
            }
        }

        @media screen and (max-width: 599px) {
            td[class=small-center] {
                text-align: center !important;
            }
        }

        @media screen and (max-width: 599px) {
            table[class=visible-lg] {
                display: none !important;
            }
        }

        @media screen and (max-width: 599px) {
            tr[class=visible-xs] {
                display: block !important;
                width: auto !important;
                overflow: visible !important;
                float: none !important;
                height: auto !important;
            }
        }

        @media screen and (max-width: 599px) {
            td[class=cols-wrapper],
            td[class=2-cols-wrapper] {
            padding-top: 18px;
        }
        }

        @media screen and (max-width: 400px) {
            td[class=2-cols-wrapper] {
            padding-left: 12px !important;
            padding-right: 12px !important;
        }
        }

        @media screen and (max-width: 400px) {
            td[class=cols-wrapper] {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
        }

        @media screen and (max-width: 400px) {
            td[class=content-wrapper] {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }

        @media screen and (max-width: 400px) {
            table[class=spacer] {
                display: none !important;
            }
        }

        @media screen and (max-width: 400px) {
            table[class=article-image-full] {
                height: 100px !important;
            }
        }

        @media screen and (max-width: 400px) {
            .article-image-full {
                height: 100px !important;
            }
        }
    </style>
    <!--[if mso]>

    <style>
        h2, h4 {
            font-family: Arial, sans-serif !important;
        }
    </style>

    <![endif]-->
</head>
<body style="margin:0; padding:0;" bgcolor="#f5f5f5" marginwidth="0" marginheight="0" margintop="0" marginleft="0" marginright="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#f5f5f5">
    <tr>
        <td align="center" valign="top" bgcolor="#f5f5f5" style="background-color: #f5f5f5;">

            <!-- row to add space above the container -->
            <table width="100%" height="40" border="0" cellpadding="0" cellspacing="0" class="spacer" style="width: 600px;border:none;" bgcolor="#f5f5f5">
                <tr>
                    <td height="40" width="100%" bgcolor="#f5f5f5" style="background-color: #f5f5f5;">
                        <br>
                    </td>
                </tr>
            </table>
            <!-- /row to add space above the container -->

            <!-- 600px container (white background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;">
                <tr>
                    <td class="content" align="left" style="background-color:#ffffff;border:none;">

                        <!-- 600px container (white background) -->
                        <table width="600" border="0" cellpadding="0" cellspacing="0" class="force-row" style="width: 600px;border:none;">
                            <tr>
                                <td align="center" valign="top" bgcolor="#ffffff">
                                    <br>
                                    <br>
                                    <table border="0" width="600" cellpadding="20" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;padding: 20px;">
                                        <tr style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;background-color:#ffffff;">
                                            <td style="text-align:left;max-width: 100%;" width="560" class="force-row">
                                                <h2>[[+name]]</h2>
                                                [[+fiarContent]]
                                            </td>
                                        </tr>
                                        <tr style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;background-color:#ffffff;">
                                            <td style="text-align:left;max-width: 100%;" width="560" class="force-row">
                                                [[+fiarFields]]
                                            </td>
                                        </tr>
                                    </table>
                                    <br>&nbsp;
                                </td>
                            </tr>
                        </table>
                        <!-- /600px container (white background) -->
                    </td>
                </tr>
            </table>
            <!--/600px container -->

            <!-- 600px container (no background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px;border:none;">
                <tr>
                    <td class="content" align="left" style="background-color:#f5f5f5;border:none;">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" class="force-row" style="width: 600px;border:none;">
                            <tr>
                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; color: #5d6561; text-align: left; padding-top: 15px;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- 600px container (no background) -->
            <br>
        </td>
    </tr>
</table>
<!--/100% background wrapper-->
</body>
</html>',
    ),
  ),
  '8ce3214f15877fc0e0c8b6ce85c02388' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldCheckboxesItemTpl',
    ),
    'object' => 
    array (
      'id' => 58,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldCheckboxesItemTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<label>
    <input type="checkbox" name="[[!+name]][]" id="[[!+name]]_[[!+idx]]" class="form-control form-control--checkbox [[!+error:notempty=`error`]]" value="[[!+title]]"
        [[!FormItIsChecked? &input=`[[!+value]]` &options=`[[!+title]]`]]
    />
    [[!+title]]
</label>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<label>
    <input type="checkbox" name="[[!+name]][]" id="[[!+name]]_[[!+idx]]" class="form-control form-control--checkbox [[!+error:notempty=`error`]]" value="[[!+title]]"
        [[!FormItIsChecked? &input=`[[!+value]]` &options=`[[!+title]]`]]
    />
    [[!+title]]
</label>',
    ),
  ),
  'fd1059dcfc724af6fd9224a28dbfeb66' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldCheckboxesTpl',
    ),
    'object' => 
    array (
      'id' => 59,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldCheckboxesTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <input type="hidden" name="[[+name]]" />
    [[!+valuesCount:gt=`1`:then=`
        <label>[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    `:else=``]]
    <div class="form-control--wrapper">
        [[!+values]]
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <input type="hidden" name="[[+name]]" />
    [[!+valuesCount:gt=`1`:then=`
        <label>[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    `:else=``]]
    <div class="form-control--wrapper">
        [[!+values]]
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
    ),
  ),
  'ab4ba94b817ad61e554c1db51485594f' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldDescriptionTpl',
    ),
    'object' => 
    array (
      'id' => 60,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldDescriptionTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<div class="form-group">
    [[!+description]]
</div>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<div class="form-group">
    [[!+description]]
</div>',
    ),
  ),
  '9b20207a316f972212c678ad116f8ceb' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldEmailTpl',
    ),
    'object' => 
    array (
      'id' => 61,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldEmailTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <input type="email" id="[[!+name]]" name="[[!+name]]" class="form-control [[!+error:notempty=`error`]]" value="[[!+value]]" [[!+placeholder:notempty=`placeholder="[[!+placeholder]]"`]] />
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <input type="email" id="[[!+name]]" name="[[!+name]]" class="form-control [[!+error:notempty=`error`]]" value="[[!+value]]" [[!+placeholder:notempty=`placeholder="[[!+placeholder]]"`]] />
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
    ),
  ),
  'd7d71c9d06f8a5e8e88cb20b4b6c1158' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldFileTpl',
    ),
    'object' => 
    array (
      'id' => 62,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldFileTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <input type="file" name="[[!+name]]" id="[[!+name]]" class="form-control form-control--file [[!+error:notempty=`error`]]" value="[[!+value.name]]" />
        [[!+error]]
        <i>[[!+value.name]]</i>
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <input type="file" name="[[!+name]]" id="[[!+name]]" class="form-control form-control--file [[!+error:notempty=`error`]]" value="[[!+value.name]]" />
        [[!+error]]
        <i>[[!+value.name]]</i>
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
    ),
  ),
  'd71541327dc3706fa5ffef5fc2cfd71e' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldHeadingTpl',
    ),
    'object' => 
    array (
      'id' => 63,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldHeadingTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<div class="form-group">
    <[[+property]]>[[+title]]</[[+property]]>
    [[+description]]
</div>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<div class="form-group">
    <[[+property]]>[[+title]]</[[+property]]>
    [[+description]]
</div>',
    ),
  ),
  '7d8e37cd2b23d4c34b816e6776ce4da6' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldNumberTpl',
    ),
    'object' => 
    array (
      'id' => 64,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldNumberTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <input type="number" id="[[!+name]]" name="[[!+name]]" class="form-control [[!+error:notempty=`error`]]" value="[[!+value]]" [[!+placeholder:notempty=`placeholder="[[!+placeholder]]"`]] />
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <input type="number" id="[[!+name]]" name="[[!+name]]" class="form-control [[!+error:notempty=`error`]]" value="[[!+value]]" [[!+placeholder:notempty=`placeholder="[[!+placeholder]]"`]] />
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
    ),
  ),
  '36ef70ff96800e6397e415593871b6ef' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldRadiobuttonsItemTpl',
    ),
    'object' => 
    array (
      'id' => 65,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldRadiobuttonsItemTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<label>
    <input type="radio" name="[[!+name]]" id="[[!+name]]_[[!+idx]]" class="form-control form-control--radio [[!+error:notempty=`error`]]" value="[[!+title]]"
        [[!FormItIsChecked? &input=`[[!+value]]` &options=`[[!+title]]`]]
    />
    [[!+title]]
</label>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<label>
    <input type="radio" name="[[!+name]]" id="[[!+name]]_[[!+idx]]" class="form-control form-control--radio [[!+error:notempty=`error`]]" value="[[!+title]]"
        [[!FormItIsChecked? &input=`[[!+value]]` &options=`[[!+title]]`]]
    />
    [[!+title]]
</label>',
    ),
  ),
  '06c36b5d548821862b027bbd0d339346' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldRadiobuttonsTpl',
    ),
    'object' => 
    array (
      'id' => 66,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldRadiobuttonsTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label>[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        [[!+values]]
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label>[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        [[!+values]]
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
    ),
  ),
  'b750f58ceae85eb1a8c2a5d0419915a7' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldSelectItemTpl',
    ),
    'object' => 
    array (
      'id' => 67,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldSelectItemTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<option value="[[!+title]]" id="[[!+name]]_[[!+idx]]"
    [[!FormItIsSelected? &input=`[[!+value]]` &options=`[[!+title]]`]]
>[[!+title]]</option>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<option value="[[!+title]]" id="[[!+name]]_[[!+idx]]"
    [[!FormItIsSelected? &input=`[[!+value]]` &options=`[[!+title]]`]]
>[[!+title]]</option>',
    ),
  ),
  'a805d10001cb0eb067c5f23d5f51a0c7' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldSelectTpl',
    ),
    'object' => 
    array (
      'id' => 68,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldSelectTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <select id="[[!+name]]" name="[[!+name]]" class="form-control form-control--select [[!+error:notempty=`error`]]">
            [[!+placeholder:notempty=`<option value="">[[+placeholder]]</option>`]]
            [[!+values]]
        </select>
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <select id="[[!+name]]" name="[[!+name]]" class="form-control form-control--select [[!+error:notempty=`error`]]">
            [[!+placeholder:notempty=`<option value="">[[+placeholder]]</option>`]]
            [[!+values]]
        </select>
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
    ),
  ),
  '5a9c6be2c0aa8c3a0c006a62d61ab75d' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldTextareaTpl',
    ),
    'object' => 
    array (
      'id' => 69,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldTextareaTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <textarea id="[[!+name]]" name="[[!+name]]" class="form-control form-control--textarea [[!+error:notempty=`error`]]" [[!+placeholder:notempty=`placeholder="[[!+placeholder]]"`]]>[[!+value]]</textarea>
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <textarea id="[[!+name]]" name="[[!+name]]" class="form-control form-control--textarea [[!+error:notempty=`error`]]" [[!+placeholder:notempty=`placeholder="[[!+placeholder]]"`]]>[[!+value]]</textarea>
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
    ),
  ),
  '60df371bbe97888213d2af5ec6e53334' => 
  array (
    'criteria' => 
    array (
      'name' => 'formaliciousFieldTextTpl',
    ),
    'object' => 
    array (
      'id' => 70,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'formaliciousFieldTextTpl',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <input type="text" id="[[!+name]]" name="[[!+name]]" class="form-control [[!+error:notempty=`error`]]" value="[[!+value]]" [[!+placeholder:notempty=`placeholder="[[!+placeholder]]"`]] />
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<div class="form-group [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    <div class="form-control--wrapper">
        <input type="text" id="[[!+name]]" name="[[!+name]]" class="form-control [[!+error:notempty=`error`]]" value="[[!+value]]" [[!+placeholder:notempty=`placeholder="[[!+placeholder]]"`]] />
        [[!+error]]
    </div>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
</div>',
    ),
  ),
  '9a5c4b5d4127c920d7343ccb09de257f' => 
  array (
    'criteria' => 
    array (
      'name' => 'FormaliciousHookHandleForm',
    ),
    'object' => 
    array (
      'id' => 93,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'FormaliciousHookHandleForm',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '/**
 * Formalicious
 *
 * Copyright 2019 by Sterc <modx@sterc.nl>
 */

$instance = $modx->getService(\'FormaliciousSnippetHookHandleForm\', \'FormaliciousSnippetHookHandleForm\', $modx->getOption(\'formalicious.core_path\', null, $modx->getOption(\'core_path\') . \'components/formalicious/\') . \'model/formalicious/snippets/\');

if ($instance instanceof FormaliciousSnippetHookHandleForm) {
    return $instance->run($hook, $errors);
}

return \'\';',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * Formalicious
 *
 * Copyright 2019 by Sterc <modx@sterc.nl>
 */

$instance = $modx->getService(\'FormaliciousSnippetHookHandleForm\', \'FormaliciousSnippetHookHandleForm\', $modx->getOption(\'formalicious.core_path\', null, $modx->getOption(\'core_path\') . \'components/formalicious/\') . \'model/formalicious/snippets/\');

if ($instance instanceof FormaliciousSnippetHookHandleForm) {
    return $instance->run($hook, $errors);
}

return \'\';',
    ),
  ),
  '4cfc85d599f59f8347038e72914ea2a3' => 
  array (
    'criteria' => 
    array (
      'name' => 'FormaliciousHookRemoveForm',
    ),
    'object' => 
    array (
      'id' => 94,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'FormaliciousHookRemoveForm',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '/**
 * Formalicious
 *
 * Copyright 2019 by Sterc <modx@sterc.nl>
 */

$instance = $modx->getService(\'FormaliciousSnippetHookRemoveForm\', \'FormaliciousSnippetHookRemoveForm\', $modx->getOption(\'formalicious.core_path\', null, $modx->getOption(\'core_path\') . \'components/formalicious/\') . \'model/formalicious/snippets/\');

if ($instance instanceof FormaliciousSnippetHookRemoveForm) {
    return $instance->run($hook, $errors);
}

return \'\';',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * Formalicious
 *
 * Copyright 2019 by Sterc <modx@sterc.nl>
 */

$instance = $modx->getService(\'FormaliciousSnippetHookRemoveForm\', \'FormaliciousSnippetHookRemoveForm\', $modx->getOption(\'formalicious.core_path\', null, $modx->getOption(\'core_path\') . \'components/formalicious/\') . \'model/formalicious/snippets/\');

if ($instance instanceof FormaliciousSnippetHookRemoveForm) {
    return $instance->run($hook, $errors);
}

return \'\';',
    ),
  ),
  '0f274d6788c7416cf632337a7b3180ce' => 
  array (
    'criteria' => 
    array (
      'name' => 'FormaliciousHookRenderForm',
    ),
    'object' => 
    array (
      'id' => 95,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'FormaliciousHookRenderForm',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '/**
 * Formalicious
 *
 * Copyright 2019 by Sterc <modx@sterc.nl>
 */

$instance = $modx->getService(\'FormaliciousSnippetHookRenderForm\', \'FormaliciousSnippetHookRenderForm\', $modx->getOption(\'formalicious.core_path\', null, $modx->getOption(\'core_path\') . \'components/formalicious/\') . \'model/formalicious/snippets/\');

if ($instance instanceof FormaliciousSnippetHookRenderForm) {
    return $instance->run($hook, $errors);
}

return \'\';',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * Formalicious
 *
 * Copyright 2019 by Sterc <modx@sterc.nl>
 */

$instance = $modx->getService(\'FormaliciousSnippetHookRenderForm\', \'FormaliciousSnippetHookRenderForm\', $modx->getOption(\'formalicious.core_path\', null, $modx->getOption(\'core_path\') . \'components/formalicious/\') . \'model/formalicious/snippets/\');

if ($instance instanceof FormaliciousSnippetHookRenderForm) {
    return $instance->run($hook, $errors);
}

return \'\';',
    ),
  ),
  '080f9e972a1bc93bf269e5c9fba27ce8' => 
  array (
    'criteria' => 
    array (
      'name' => 'FormaliciousRenderForm',
    ),
    'object' => 
    array (
      'id' => 96,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'FormaliciousRenderForm',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'snippet' => '/**
 * Formalicious
 *
 * Copyright 2019 by Sterc <modx@sterc.nl>
 */

$instance = $modx->getService(\'FormaliciousSnippetRenderForm\', \'FormaliciousSnippetRenderForm\', $modx->getOption(\'formalicious.core_path\', null, $modx->getOption(\'core_path\') . \'components/formalicious/\') . \'model/formalicious/snippets/\');

if ($instance instanceof FormaliciousSnippetRenderForm) {
    return $instance->run($scriptProperties);
}

return \'\';',
      'locked' => 0,
      'properties' => 'a:8:{s:7:"tplForm";a:7:{s:4:"name";s:7:"tplForm";s:4:"desc";s:33:"formalicious.snippet_tplform_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:36:"@FILE elements/chunks/form.chunk.tpl";s:7:"lexicon";s:23:"formalicious:properties";s:4:"area";s:0:"";}s:7:"tplStep";a:7:{s:4:"name";s:7:"tplStep";s:4:"desc";s:33:"formalicious.snippet_tplstep_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:36:"@FILE elements/chunks/step.chunk.tpl";s:7:"lexicon";s:23:"formalicious:properties";s:4:"area";s:0:"";}s:17:"tplNavigationItem";a:7:{s:4:"name";s:17:"tplNavigationItem";s:4:"desc";s:43:"formalicious.snippet_tplnavigationitem_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:47:"@FILE elements/chunks/navigation/item.chunk.tpl";s:7:"lexicon";s:23:"formalicious:properties";s:4:"area";s:0:"";}s:20:"tplNavigationWrapper";a:7:{s:4:"name";s:20:"tplNavigationWrapper";s:4:"desc";s:46:"formalicious.snippet_tplnavigationwrapper_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:50:"@FILE elements/chunks/navigation/wrapper.chunk.tpl";s:7:"lexicon";s:23:"formalicious:properties";s:4:"area";s:0:"";}s:8:"tplEmail";a:7:{s:4:"name";s:8:"tplEmail";s:4:"desc";s:34:"formalicious.snippet_tplemail_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:20:"formaliciousEmailTpl";s:7:"lexicon";s:23:"formalicious:properties";s:4:"area";s:0:"";}s:18:"tplEmailFieldsItem";a:7:{s:4:"name";s:18:"tplEmailFieldsItem";s:4:"desc";s:44:"formalicious.snippet_tplemailfieldsitem_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:42:"@FILE elements/chunks/email/item.chunk.tpl";s:7:"lexicon";s:23:"formalicious:properties";s:4:"area";s:0:"";}s:21:"tplEmailFieldsWrapper";a:7:{s:4:"name";s:21:"tplEmailFieldsWrapper";s:4:"desc";s:47:"formalicious.snippet_tplemailfieldswrapper_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:45:"@FILE elements/chunks/email/wrapper.chunk.tpl";s:7:"lexicon";s:23:"formalicious:properties";s:4:"area";s:0:"";}s:12:"tplFiarEmail";a:7:{s:4:"name";s:12:"tplFiarEmail";s:4:"desc";s:38:"formalicious.snippet_tplfiaremail_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:24:"formaliciousFiarEmailTpl";s:7:"lexicon";s:23:"formalicious:properties";s:4:"area";s:0:"";}}',
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * Formalicious
 *
 * Copyright 2019 by Sterc <modx@sterc.nl>
 */

$instance = $modx->getService(\'FormaliciousSnippetRenderForm\', \'FormaliciousSnippetRenderForm\', $modx->getOption(\'formalicious.core_path\', null, $modx->getOption(\'core_path\') . \'components/formalicious/\') . \'model/formalicious/snippets/\');

if ($instance instanceof FormaliciousSnippetRenderForm) {
    return $instance->run($scriptProperties);
}

return \'\';',
    ),
  ),
  'b2cc81796d82af3bf343b87a9806218a' => 
  array (
    'criteria' => 
    array (
      'name' => 'Formalicious',
    ),
    'object' => 
    array (
      'id' => 20,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'Formalicious',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'cache_type' => 0,
      'plugincode' => '/**
 * Formalicious
 *
 * Copyright 2019 by Sterc <modx@sterc.nl>
 */

if ($modx->event->name === \'ContentBlocks_RegisterInputs\') {
    require_once $modx->getOption(\'formalicious.core_path\', null, MODX_CORE_PATH . \'components/formalicious/\') . \'elements/inputs/FormaliciousInput.php\';

    $instance = new FormaliciousInput($contentBlocks);

    if ($instance instanceof FormaliciousInput) {
        $modx->event->output([
            \'formalicious\' => $instance
        ]);
    }
}',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * Formalicious
 *
 * Copyright 2019 by Sterc <modx@sterc.nl>
 */

if ($modx->event->name === \'ContentBlocks_RegisterInputs\') {
    require_once $modx->getOption(\'formalicious.core_path\', null, MODX_CORE_PATH . \'components/formalicious/\') . \'elements/inputs/FormaliciousInput.php\';

    $instance = new FormaliciousInput($contentBlocks);

    if ($instance instanceof FormaliciousInput) {
        $modx->event->output([
            \'formalicious\' => $instance
        ]);
    }
}',
    ),
  ),
  '5ecf0fbe95497fc57a501b8c7315a18d' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 20,
      'event' => 'ContentBlocks_RegisterInputs',
    ),
    'object' => 
    array (
      'pluginid' => 20,
      'event' => 'ContentBlocks_RegisterInputs',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'cb99838dcf9c8e96f4367fac84bcca41' => 
  array (
    'criteria' => 
    array (
      'name' => 'formalicious',
    ),
    'object' => 
    array (
      'id' => 9,
      'source' => 0,
      'property_preprocess' => 0,
      'type' => 'listbox',
      'name' => 'formalicious',
      'caption' => 'Formalicious form',
      'description' => '',
      'editor_type' => 0,
      'category' => 48,
      'locked' => 0,
      'elements' => '@SELECT \'- Select a form -\' AS name, 0 AS id UNION ALL SELECT name,id FROM [[+PREFIX]]formalicious_forms WHERE published = 1',
      'rank' => 0,
      'display' => '',
      'default_text' => '',
      'properties' => 'a:0:{}',
      'input_properties' => 'a:1:{s:10:"allowBlank";b:0;}',
      'output_properties' => NULL,
      'static' => 0,
      'static_file' => '',
      'content' => '',
    ),
  ),
  '58e20b0b9c950057da7584c94d1d39d5' => 
  array (
    'criteria' => 
    array (
      'text' => 'formalicious',
    ),
    'object' => 
    array (
      'text' => 'formalicious',
      'parent' => 'topnav',
      'action' => 'home',
      'description' => '',
      'icon' => '',
      'menuindex' => 3,
      'params' => '',
      'handler' => '',
      'permissions' => 'formalicious',
      'namespace' => 'formalicious',
    ),
  ),
);