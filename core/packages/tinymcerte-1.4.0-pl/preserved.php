<?php return array (
  'df0bc069b044acba72f9dab3f7701d94' => 
  array (
    'criteria' => 
    array (
      'name' => 'tinymcerte',
    ),
    'object' => 
    array (
      'name' => 'tinymcerte',
      'path' => '{core_path}components/tinymcerte/',
      'assets_path' => '{assets_path}components/tinymcerte/',
    ),
  ),
  '6cd7e3045b9995f1037dd1e82afb3d45' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.plugins',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.plugins',
      'value' => 'advlist autolink lists modximage charmap print preview anchor visualblocks searchreplace code fullscreen insertdatetime media table contextmenu paste modxlink',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '2016-02-18 13:54:25',
    ),
  ),
  '535d882f2951eb1035da8c0a785d75a4' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.menubar',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.menubar',
      'value' => 'file edit insert view format table tools',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'bf6c11bbcdb5c4624e0de6492b42a17c' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.statusbar',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.statusbar',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '4a0b2c0d6a2dd5c13b07bb0be9aa16ca' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.image_advtab',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.image_advtab',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '13b2db374fa541f1c7de72f97c2ebb79' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.object_resizing',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.object_resizing',
      'value' => '1',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'a5b00ce0b1b482d898a41b22f7395825' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.paste_as_text',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.paste_as_text',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '28c2bf24424181d40ae99a4fc70e8453' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.link_class_list',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.link_class_list',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '73f3ca679e59eeee32204d3bd4946445' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.browser_spellcheck',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.browser_spellcheck',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '5796294b44b01be2ee4a5ad2501af527' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.content_css',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.content_css',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '3ae2c9051ed8aec546f04b1423f59e45' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.image_class_list',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.image_class_list',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '5b659dc77b4fc7da01fb1428224ad8a9' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.external_config',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.external_config',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'c7c0a4530a0ac514854797ad1cf9dcee' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.skin',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.skin',
      'value' => 'modx',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '089236ee1a6f4e837a5b48f2712d9073' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.relative_urls',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.relative_urls',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '2b78e48f7e62fc70a0620ade27a686e3' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.remove_script_host',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.remove_script_host',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'ee755f90b3b3ba8da3e044fc9dfb1af0' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.valid_elements',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.valid_elements',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '9059113ae428352a4157e34a8a3e2022' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.links_across_contexts',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.links_across_contexts',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'default',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '375278ff7565488549fb2e7bb6b45828' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.toolbar1',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.toolbar1',
      'value' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.toolbar',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'ffabf0b054de940ed82f7f703908ef48' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.toolbar2',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.toolbar2',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.toolbar',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '788b45a2176362b744259a889eda60cf' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.toolbar3',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.toolbar3',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.toolbar',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '40b1712f90419d49833446f4b4934f72' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.style_formats',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.style_formats',
      'value' => '[{"title": "Headers", "items": "headers_format"},{"title": "Inline", "items": "inline_format"},{"title": "Blocks", "items": "blocks_format"},{"title": "Alignment", "items": "alignment_format"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '287eca500ae48cf3bdd75d98a5f7e7e2' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.headers_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.headers_format',
      'value' => '[{"title": "Header 1", "format": "h1"},{"title": "Header 2", "format": "h2"},{"title": "Header 3", "format": "h3"},{"title": "Header 4", "format": "h4"},{"title": "Header 5", "format": "h5"},{"title": "Header 6", "format": "h6"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '60831fde7b3bda530d81ff8e250ef62c' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.inline_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.inline_format',
      'value' => '[{"title": "Bold", "icon": "bold", "format": "bold"},{"title": "Italic", "icon": "italic", "format": "italic"},{"title": "Underline", "icon": "underline", "format": "underline"},{"title": "Strikethrough", "icon": "strikethrough", "format": "strikethrough"},{"title": "Superscript", "icon": "superscript", "format": "superscript"},{"title": "Subscript", "icon": "subscript", "format": "subscript"},{"title": "Code", "icon": "code", "format": "code"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'c0d9b802ed8381b96aa075033f50706c' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.blocks_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.blocks_format',
      'value' => '[{"title": "Paragraph", "format": "p"},{"title": "Blockquote", "format": "blockquote"},{"title": "Div", "format": "div"},{"title": "Pre", "format": "pre"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '755b43a470b5c20c02c7613d42cabe26' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.alignment_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.alignment_format',
      'value' => '[{"title": "Left", "icon": "alignleft", "format": "alignleft"},{"title": "Center", "icon": "aligncenter", "format": "aligncenter"},{"title": "Right", "icon": "alignright", "format": "alignright"},{"title": "Justify", "icon": "alignjustify", "format": "alignjustify"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '14dac6c019af2a559345773fb8f0c2cc' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.style_formats_merge',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.style_formats_merge',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '932baed5b9d0995e52ecdc817925f107' => 
  array (
    'criteria' => 
    array (
      'category' => 'TinyMCE Rich Text Editor',
    ),
    'object' => 
    array (
      'id' => 23,
      'parent' => 0,
      'category' => 'TinyMCE Rich Text Editor',
      'rank' => 0,
    ),
  ),
  '121b3af75b3dee418211cba66d4b2ced' => 
  array (
    'criteria' => 
    array (
      'name' => 'TinyMCERTE',
    ),
    'object' => 
    array (
      'id' => 10,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'TinyMCERTE',
      'description' => '',
      'editor_type' => 0,
      'category' => 23,
      'cache_type' => 0,
      'plugincode' => '/**
 * TinyMCE Rich Tech Editor
 *
 */
$corePath = $modx->getOption(\'tinymcerte.core_path\', null, $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/tinymcerte/\');
/** @var TinyMCERTE $tinymcerte */
$tinymcerte = $modx->getService(
    \'tinymcerte\',
    \'TinyMCERTE\',
    $corePath . \'model/tinymcerte/\',
    array(
        \'core_path\' => $corePath
    )
);

$className = \'TinyMCERTE\' . $modx->event->name;
$modx->loadClass(\'TinyMCERTEPlugin\', $tinymcerte->getOption(\'modelPath\') . \'tinymcerte/events/\', true, true);
$modx->loadClass($className, $tinymcerte->getOption(\'modelPath\') . \'tinymcerte/events/\', true, true);
if (class_exists($className)) {
    /** @var TinyMCERTEPlugin $handler */
    $handler = new $className($modx, $scriptProperties);
    $handler->run();
}
return;',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * TinyMCE Rich Tech Editor
 *
 */
$corePath = $modx->getOption(\'tinymcerte.core_path\', null, $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/tinymcerte/\');
/** @var TinyMCERTE $tinymcerte */
$tinymcerte = $modx->getService(
    \'tinymcerte\',
    \'TinyMCERTE\',
    $corePath . \'model/tinymcerte/\',
    array(
        \'core_path\' => $corePath
    )
);

$className = \'TinyMCERTE\' . $modx->event->name;
$modx->loadClass(\'TinyMCERTEPlugin\', $tinymcerte->getOption(\'modelPath\') . \'tinymcerte/events/\', true, true);
$modx->loadClass($className, $tinymcerte->getOption(\'modelPath\') . \'tinymcerte/events/\', true, true);
if (class_exists($className)) {
    /** @var TinyMCERTEPlugin $handler */
    $handler = new $className($modx, $scriptProperties);
    $handler->run();
}
return;',
    ),
  ),
  '4a4d5df35d8124269bbfd8059edc3fdb' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextBrowserInit',
    ),
    'object' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextBrowserInit',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'ff97b2f1c9ce318b6a12893376c38a4e' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorRegister',
    ),
    'object' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorRegister',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '5f46ae7dfa896133d8dd056ac5e7f93c' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorInit',
    ),
    'object' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorInit',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);