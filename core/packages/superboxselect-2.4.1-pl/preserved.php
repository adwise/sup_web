<?php return array (
  'fe281ff3d4792bc6e46ca2077662608b' => 
  array (
    'criteria' => 
    array (
      'name' => 'superboxselect',
    ),
    'object' => 
    array (
      'name' => 'superboxselect',
      'path' => '{core_path}components/superboxselect/',
      'assets_path' => '{assets_path}components/superboxselect/',
    ),
  ),
  'fb41d1526398a77a8c04d364d5e6456e' => 
  array (
    'criteria' => 
    array (
      'key' => 'superboxselect.debug',
    ),
    'object' => 
    array (
      'key' => 'superboxselect.debug',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'superboxselect',
      'area' => 'system',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '307db0ab148a1762b8019ed1130b148f' => 
  array (
    'criteria' => 
    array (
      'key' => 'superboxselect.advanced',
    ),
    'object' => 
    array (
      'key' => 'superboxselect.advanced',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'superboxselect',
      'area' => 'system',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '8a75b1864e0b5289d4b87c381f3cd293' => 
  array (
    'criteria' => 
    array (
      'category' => 'SuperBoxSelect',
    ),
    'object' => 
    array (
      'id' => 47,
      'parent' => 0,
      'category' => 'SuperBoxSelect',
      'rank' => 0,
    ),
  ),
  '0e9eb82cbb6216dbb6e4eb67b643c30b' => 
  array (
    'criteria' => 
    array (
      'name' => 'SuperBoxSelect',
    ),
    'object' => 
    array (
      'id' => 19,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'SuperBoxSelect',
      'description' => 'SuperBoxSelect runtime hooks - registers custom TV input types and includes javascripts on document edit pages.',
      'editor_type' => 0,
      'category' => 47,
      'cache_type' => 0,
      'plugincode' => '/**
 * SuperBoxSelect Runtime Hooks
 *
 * Registers custom TV input types and includes javascripts on
 * document edit pages so that the TV can be used from within other extras
 * (i.e. MIGX, Collections)
 *
 * @package superboxselect
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVInputPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$corePath = $modx->getOption(\'superboxselect.core_path\', null, $modx->getOption(\'core_path\') . \'components/superboxselect/\');
/** @var SuperBoxSelect $superboxselect */
$superboxselect = $modx->getService(\'superboxselect\', \'SuperBoxSelect\', $corePath . \'model/superboxselect/\', array(
    \'core_path\' => $corePath
));

switch ($modx->event->name) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'superboxselect:default\');
        $tvId = isset($modx->controller->scriptProperties[\'id\']) ? $modx->controller->scriptProperties[\'id\'] : 0;
        /** @var modTemplateVar $tv */
        $tv = $modx->getObject(\'modTemplateVar\', $tvId);
        if ($tv) {
            $tvProperties = $tv->get(\'input_properties\');
            $package = isset($tvProperties[\'selectPackage\']) ? $tvProperties[\'selectPackage\'] : \'\';
        } else {
            $package = \'\';
        }
        $superboxselect->includeScriptAssets($package);
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnDocFormRender\':
        $superboxselect->includeScriptAssets();
        break;
};',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * SuperBoxSelect Runtime Hooks
 *
 * Registers custom TV input types and includes javascripts on
 * document edit pages so that the TV can be used from within other extras
 * (i.e. MIGX, Collections)
 *
 * @package superboxselect
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVInputPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$corePath = $modx->getOption(\'superboxselect.core_path\', null, $modx->getOption(\'core_path\') . \'components/superboxselect/\');
/** @var SuperBoxSelect $superboxselect */
$superboxselect = $modx->getService(\'superboxselect\', \'SuperBoxSelect\', $corePath . \'model/superboxselect/\', array(
    \'core_path\' => $corePath
));

switch ($modx->event->name) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'superboxselect:default\');
        $tvId = isset($modx->controller->scriptProperties[\'id\']) ? $modx->controller->scriptProperties[\'id\'] : 0;
        /** @var modTemplateVar $tv */
        $tv = $modx->getObject(\'modTemplateVar\', $tvId);
        if ($tv) {
            $tvProperties = $tv->get(\'input_properties\');
            $package = isset($tvProperties[\'selectPackage\']) ? $tvProperties[\'selectPackage\'] : \'\';
        } else {
            $package = \'\';
        }
        $superboxselect->includeScriptAssets($package);
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnDocFormRender\':
        $superboxselect->includeScriptAssets();
        break;
};',
    ),
  ),
  '51902f5faed272e5df6c2c4eefba758d' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnManagerPageBeforeRender',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnManagerPageBeforeRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '1b57904b1343be18816e9746e475c39e' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputPropertiesList',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputPropertiesList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '1f59ad5c3f9d20a7776b7cb11298a6e2' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputRenderList',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputRenderList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'ff51289830618411773ffc6e77deb7b6' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnDocFormRender',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnDocFormRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);