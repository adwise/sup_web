<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'license' => 'Formalicious is proprietary software, developed by Sterc and distributed through modmore.com. By purchasing Formalicious via https://www.modmore.com/formalicious/, you have received a usage license for a single (1) MODX Revolution installation, including one year (starting on date of purchase) of email support.

While we hope Formalicious is useful to you and we will try to help you successfully use Formalicious, modmore or Sterc is not liable for loss of revenue, data, damages or other financial loss resulting from the installation or use of Formalicious.

By using and installing this package, you acknowledge that you shall only use this on a single MODX installation.

Redistribution in any shape or form is strictly prohibited. You may customize or change the provided source code to tailor Formalicious for your own use, as long as no attempt is made to remove license protection measures. By changing source code you acknowledge you void the right to support unless coordinated with modmore support.
',
    'readme' => '--------------------
Formalicious
--------------------
Author: Sterc <modx@sterc.nl>
--------------------

Formalicious is the most powerful and easiest MODX form builder, with built-in multi-step forms, 8 field types, validation and the ability to use hooks and other advanced FormIt features.

Important! Since Formalicious 2.0.0 we refactored some snippets and chunks. The snippet RenderForm is replaced with FormaliciousRenderForm.
Please check your snippet and the script properties and check if the chunks/templates of the \'Fieldtypes\' are correct.
',
    'changelog' => 'Version 2.0.3-pl
- Set required integers by default

Version 2.0.2-pl
- Set currentUrl with step=1 when steps are there
- Make the id column on fields visible

Version 2.0.1-pl
--------------------------
- Add sterc extra settings when not existing

Version 2.0.0-pl
--------------------------
- Add form DB fields published_from and published_till to auto (de)publication forms
- Add field answer DB field selected to set default selected
- Rich text support for field description and form email content.
- Steps (navigation) above the form
- New parameter stepRedirect to redirect a step to a non resource ID (if stepRedirect is set to \'request\' the step will be redirected to the current REQUEST URL)
- New permissions added
    - formalicious_admin to show/hide admin panel
    - formalicious_tab_fields to show/hide fields tab
    - formalicious_tab_advanced to show/hide advanced tab (formalicious_advanced renamed to formalicious_tab_advanced)
- ExtJS refactored for faster and better UI/UX
    - Step preview fixed
    - Toggleable description, placeholder, required and heading fields for each fieldtype
- RenderForm replaced with FormaliciousRenderForm
- All snippets and chunks are prefixed with Formalicious

Version 1.4.1-pl
--------------------------
- Create database fields on update

Version 1.4.0-pl
--------------------------
- Add field description
- Hide advanced tab based on permissions
- Add heading & description fields
- Add field description
- Change fiarcontent from varchar to text for bigger mails

Version 1.3.1-pl
--------------------------
- Add system setting for disable form saving on install
- Change fiarcontent from varchar to text

Version 1.3.0-pl
--------------------------
- Fixed phptype of some fields in schema of tables (PHP 7 compatibility)
- Added system setting to disable overall form saving functionality
- Added russian lexicon

Version 1.2.1-pl (October 2017)
--------------------------
- Remove the limit from the ContentBlocks input field
- Hide autoreply options when autoreply checkbox is unchecked

Version 1.2.0-pl (August 2nd, 2017)
--------------------------
- Removing default limit from fiaremailto field (#31)
- Add back button to form update view
- Add duplicate option to forms grid (#32)
- Update grid action buttons to use modx font-awesome icons
- Make add step/field buttons more visible
- Add preview option to form fields tab
- Add saveTmpFiles FormIt property to default formTpl
- Add formName FormIt property to default formTpl
- Prefix fiar-attachment field with modx base_path
- Only add email hook when emailto is not empty
- Remove default limit of 20 from field-values grid
- Check for common \'spam,email,redirect\' hooks added by Formalicious when saving posthooks
- Add ID field to form-fields grid
- Make sure prehooks are run before the renderForm snippet

Version 1.1.0-pl (April 19th, 2017)
--------------------------
- Fix setting placeholder for stepParam parameter for renderForm
- Show message when trying to display unpublished form (#6)
- Update radio and checkbox chunks to use correct Bootstrap classes (#28)
- Allow emailTpl and fiarTpl to be overwritten with renderForm snippet parameters (#23)
- Add validate and customValidators parameters to renderForm and formTpl (#23)

Version 1.0.1-pl (February 3rd, 2017)
--------------------------
- Added ContentBlocks support (thanks Mark!)
- Fixed installation issues with MODX installations with custom table-prefixes

Version 1.0.0-pl (February 1st, 2017)
--------------------------
- XS-4 New documentation
- XS-11 Changed height of several dialog windows
- XS-12 Spacing adjustments
- XS-19 Gave the default emails a lighter grey
- XS-20 Modified all en/nl lexicons
- XS-21 Fixed inline editing (removed it)

Version 1.0.0-RC2 (January 27th, 2017)
--------------------------
- [#28] Fixed oldstyle actions
- [#29] Improved this very changelog
- [#40] Create a readme
- [#41] New logo for the modmore site!
- [#XS-42] Autoheight for new-field dialog

Version 1.0.0-RC1 (January 26th, 2017)
--------------------------
- [#34] Improved handling of empty fields
- [#37] Radio button # Select # Checkbox options are now required
- [#38] Allowed files are now mentioned
- [#36] Improved default emails
- [#32] Unused description field is now removed
- [#31] Improved placeholder field usage
- [#30] Mention context-NAME in the "Redirect to" field when creating a new form
- [#27] Fixed file upload in multistep form
- [#22] Improved emailTpl
- [#20 + #23 + #35] Improved styling of buttons
- [#17] Fixed category_id fallback
- [#9 + #12] Fixed empty fields in multistep form
- [#13] Fixed email validation
- [#10] Fixed adding parameters not working properly
- [#7] Now shipped with TV
- [#8] Fixed uninstallation proces
- [#4] "Update type" dialog is now bigger
- [#2] Fixed select form-email-field when creating a form
- [#1] Fixed empty field when creating a form
- [#6] Improved adding fields
- [#5] Improved step-creation flow
- [#3] Replaced form-description with "Email header text"

Version 0.6.0 (2016)
--------------------------
- Create form categories
- Ability to create form steps
- Ability to save forms in FormIt (FormIt V2.2.2#) CMP
- Added ability to setup autoresponder in form
- Updated lexicons
',
    'setup-options' => 'formalicious-2.0.3-pl/setup-options.php',
    'requires' => 
    array (
      'formit' => '>=4.1.1',
    ),
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => '4f3827e0f0a0dd32090091196012fca7',
      'native_key' => 'formalicious',
      'filename' => 'modNamespace/94c753c2447d3d3e9da4c850f0900d2f.vehicle',
      'namespace' => 'formalicious',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'e27266036e9642eb094a0e75114b7689',
      'native_key' => 'formalicious.branding_url',
      'filename' => 'modSystemSetting/20768c97a1dfc11a646917096a4830da.vehicle',
      'namespace' => 'formalicious',
    ),
    2 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'de40c78aed6d57168882f41c10f36687',
      'native_key' => 'formalicious.branding_url_help',
      'filename' => 'modSystemSetting/909eea338d56b6c2ff689c28fee18734.vehicle',
      'namespace' => 'formalicious',
    ),
    3 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd79dade1dab41fcfc0dd0374db44ee3f',
      'native_key' => 'formalicious.saveforms',
      'filename' => 'modSystemSetting/fc645377df0d56560ba41bdf2cd93130.vehicle',
      'namespace' => 'formalicious',
    ),
    4 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '68459c833ef4f5c60346ebd52b925aa7',
      'native_key' => 'formalicious.saveforms_prefix',
      'filename' => 'modSystemSetting/f946a10e17286f4e5c949c883ba4bd00.vehicle',
      'namespace' => 'formalicious',
    ),
    5 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'fefaa534b5bea799a02960058082bacb',
      'native_key' => 'formalicious.disallowed_hooks',
      'filename' => 'modSystemSetting/b5dc3192fb9fdd22ad578e3af7d2c851.vehicle',
      'namespace' => 'formalicious',
    ),
    6 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'c13cdcae30b6f331b1b7c6a81862e0ea',
      'native_key' => 'formalicious.preview_css',
      'filename' => 'modSystemSetting/c1ab5d21c3269db82fee65cf11aa3915.vehicle',
      'namespace' => 'formalicious',
    ),
    7 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd1ffd2491cee39d27307921124f35919',
      'native_key' => 'formalicious.source',
      'filename' => 'modSystemSetting/7c1109d281d7f3c9b38a1ee40abf73f8.vehicle',
      'namespace' => 'formalicious',
    ),
    8 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '08ff467a16dc66a143f20e508239e2e8',
      'native_key' => 'formalicious.use_editor',
      'filename' => 'modSystemSetting/234d9398fdac58b8044a093978f96990.vehicle',
      'namespace' => 'formalicious',
    ),
    9 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '7c191d9d7d73e67316bafac4b0668061',
      'native_key' => 'formalicious.editor_menubar',
      'filename' => 'modSystemSetting/2699d077c1fb485d01709703dcee4713.vehicle',
      'namespace' => 'formalicious',
    ),
    10 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'f8aa2ef0d1e9b0766d54f744b4e1b813',
      'native_key' => 'formalicious.editor_plugins',
      'filename' => 'modSystemSetting/5ac81f7b1318b06f284b0669acdbe6ff.vehicle',
      'namespace' => 'formalicious',
    ),
    11 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '9b57be89de24af25e1558503238b659f',
      'native_key' => 'formalicious.editor_statusbar',
      'filename' => 'modSystemSetting/04941674184c54bdf683dca97d46cf0c.vehicle',
      'namespace' => 'formalicious',
    ),
    12 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '2efede8221a712cb9044ccd62bea2cf7',
      'native_key' => 'formalicious.editor_toolbar1',
      'filename' => 'modSystemSetting/9930ac7afa6b293d9c08e2b70c0c0301.vehicle',
      'namespace' => 'formalicious',
    ),
    13 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '4247ae09316f3ca2ab4ec0495b49b694',
      'native_key' => 'formalicious.editor_toolbar2',
      'filename' => 'modSystemSetting/25bbb18478ee6f68c72097cbdac15ed7.vehicle',
      'namespace' => 'formalicious',
    ),
    14 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '03a033685d7ff54dfc00fb62379f8b0d',
      'native_key' => 'formalicious.editor_toolbar3',
      'filename' => 'modSystemSetting/337cebe6a0780d666bf4e2fc95da53ef.vehicle',
      'namespace' => 'formalicious',
    ),
    15 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => 'd237fa50d9fb2a5161ab5490b4c7f58a',
      'native_key' => NULL,
      'filename' => 'modCategory/bef12ce18e1e5b2ba0d49cad6ea04266.vehicle',
      'namespace' => 'formalicious',
    ),
    16 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modMenu',
      'guid' => '63dd1b89c16691ebd79eeabd705e9993',
      'native_key' => 'formalicious',
      'filename' => 'modMenu/ea380627bb9a9d59e69b957c7d64ab1c.vehicle',
      'namespace' => 'formalicious',
    ),
  ),
);