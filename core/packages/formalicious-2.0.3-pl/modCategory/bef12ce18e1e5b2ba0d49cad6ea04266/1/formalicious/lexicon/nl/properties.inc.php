<?php

/**
 * Formalicious
 *
 * Copyright 2019 by Sterc <modx@sterc.nl>
 */

$_lang['formalicious']                                          = 'Formalicious';

$_lang['formalicious.snippet_tplform_desc']                     = 'De template van het formulier, dit kan een chunk naam, @FILE of @INLINE zijn.';
$_lang['formalicious.snippet_tplstep_desc']                     = 'De template van een stap, dit kan een chunk naam, @FILE of @INLINE zijn.';
$_lang['formalicious.snippet_tplnavigationitem_desc']           = 'De template van een stap in de stappen navigatie, dit kan een chunk naam, @FILE of @INLINE zijn.';
$_lang['formalicious.snippet_tplnavigationwrapper_desc']        = 'De template van de stappen navigatie, dit kan een chunk naam, @FILE of @INLINE zijn.';
$_lang['formalicious.snippet_tplemail_desc']                    = 'De template van de email, dit kan alleen een chunk naam zijn.';
$_lang['formalicious.snippet_tplemailfieldsitem_desc']          = 'De template van de veld in de email, dit kan een chunk naam, @FILE of @INLINE zijn.';
$_lang['formalicious.snippet_tplemailfieldswrapper_desc']       = 'De template van de wrapper van velden in de email, dit kan een chunk naam, @FILE of @INLINE zijn.';
$_lang['formalicious.snippet_tplfiaremail_desc']                = 'De template van de auto-reply email, dit kan alleen een chunk naam zijn.';
