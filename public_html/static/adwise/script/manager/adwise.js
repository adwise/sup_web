Ext.namespace('MODx');
Ext.extend(MODx, Ext.Component, {
	adwiseConsole: function(action) {
		var topic = '/adwise-console/';
		this.console = MODx.load({
			xtype: 'modx-console'
			, register: 'mgr'
			, topic: topic
			, clear: true
			, show_filename: 0
			, listeners: {
				'shutdown': {
					fn: function() {

					}, scope: this
				}
			}
		});

		this.console.show(Ext.getBody());

		MODx.Ajax.request({
			url: MODx.config['adw.url_connector']
			, params: {
				location: 'manager'
				, action: action

				, register: 'mgr'
				, topic: topic
				, menu: true
				, action_map: true
			}
			, listeners: {
				'success': {
					fn: function() {
						this.console.fireEvent('complete');
					}, scope: this
				}
			}
		});
		return true;
	}
});
