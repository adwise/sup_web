<?php

define('MODX_REQP', false);

require_once(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/config.core.php');
require_once(MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php');
require_once(MODX_CONNECTORS_PATH . 'index.php');

$_SERVER['HTTP_MODAUTH'] = $_REQUEST['HTTP_MODAUTH'] = 0;

// commandline
if(isset($argv)) {
	$_GET['call'] = $argv[1];
}

/** @var modX $modx */

$modx->request->handleRequest(
	array(
		'action' => 'index',
		'location' => 'script',
		'processors_path' => $modx->getOption('core_path') . 'components/adwise/processors/'
	)
);