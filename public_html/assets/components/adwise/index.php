<?php

require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php');
require_once(MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php');
require_once(MODX_CONNECTORS_PATH . 'index.php');

/** @var modX $modx */
/** @var Adwise $adwise */
$adwise =& $modx->adwise;

/** @var string $action */
/** @var string $context */
/** @var string $location */
$location = $adwise->option('location:lcase:trim', $_REQUEST, 'frontend');

$modx->request->handleRequest(
	array(
		'action' => $action,
		'location' => $location,
		'processors_path' => $modx->getOption('core_path') . 'components/adwise/processors/'
	)
);