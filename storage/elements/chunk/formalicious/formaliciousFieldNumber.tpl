<div class="field [[!+error:notempty=`error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
    <input type="number" id="[[!+name]]" name="[[!+name]]" class="form-control [[!+required:notempty=`required`:empty=``]] [[!+error:notempty=`error`]]" [[!+required:notempty=`required`:empty=``]] value="[[!+value]]" [[!+placeholder:notempty=`placeholder="[[!+placeholder]]"`]] />
    [[!+error]]
</div>