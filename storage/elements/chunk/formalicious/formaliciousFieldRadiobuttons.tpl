<div class="field [[!+error:notempty=`error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
    <div class="form-control--list">
        [[!+values]]
    </div>
    [[!+error]]
</div>