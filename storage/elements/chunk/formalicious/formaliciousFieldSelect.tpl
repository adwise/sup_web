<div class="field [[!+error:notempty=`error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
    <select id="[[!+name]]" name="[[!+name]]" class="form-control form-control--select [[!+required:notempty=`required`:empty=``]] [[!+error:notempty=`error`]]" [[!+required:notempty=`required`:empty=``]]>
        [[!+placeholder:notempty=`<option value="">[[+placeholder]]</option>`]]
        [[!+values]]
    </select>
    [[!+error]]
</div>