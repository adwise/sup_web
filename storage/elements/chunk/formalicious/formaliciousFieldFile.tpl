<div class="field [[!+error:notempty=`error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
    <input type="file" name="[[!+name]]" id="[[!+name]]" class="form-control form-control--file [[!+required:notempty=`required`:empty=``]] [[!+error:notempty=`error`]]" [[!+required:notempty=`required`:empty=``]] value="[[!+value.name]]" />
    [[!+error]]
    <i>[[!+value.name]]</i>
</div>