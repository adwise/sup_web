<div class="field [[!+error:notempty=`has-error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
    <input type="text" id="[[!+name]]" name="[[!+name]]" class="form-control [[!+required:notempty=`required`:empty=``]] [[!+error:notempty=`error`]]" value="[[!+value]]" [[!+placeholder:notempty=`placeholder="[[!+placeholder]]"`]] />
    [[!+error]]
</div>