[[!FormIt?
[[!+FormItParameters]]
]]

<form action="[[!+currentUrl]]" class="validate forma-form [[!+fi.validation_error_message:notempty=`error`]]" id="form-[[!+id]]" method="POST" enctype="multipart/form-data" accept-charset="utf-8">
    [[!+formalicious.navigation]]
    [[!+formalicious.form]]

    <div class="form-pagination">
        [[!+step:neq=`1`:then=`<a href="[[!+prevUrl]]" class="btn btn--prev" title="[[%formalicious.prev? &namespace=`formalicious` &topic=`default`]]">
            [[%formalicious.prev? &namespace=`formalicious` &topic=`default`]]
        </a>`:else=``]]

    </div>
    <div class="form-control--submit">
        <div class="grid-x">
            <div class="cell small-12 xlarge-8">
                <p>[[%Velden met een * zijn verplicht]]</p>
            </div>
            <div class="cell small-12 auto text-right">
                <button type="submit" class="button button--primary" name="[[!+submitVar]]" value="[[!+submitVar]]" title="[[!+submitTitle]]">
                    [[!+submitTitle]] <span class="button__icon"></span
                </button>
            </div>
        </div>
    </div>

    [[!recaptchav3_render]]
    [[!+fi.error.recaptchav3_error]]

</form>