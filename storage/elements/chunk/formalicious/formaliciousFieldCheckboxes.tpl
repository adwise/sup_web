<div class="field [[!+error:notempty=`error`]]">
    <input type="hidden" name="[[+name]]" />
    <label>[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
    <div class="form-control--list">
        [[!+values]]
    </div>
    [[!+error]]
</div>