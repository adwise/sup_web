<div class="field [[!+error:notempty=`error`]]">
    <label for="[[!+name]]">[[!+title]][[!+required:notempty=`*`:empty=``]]:</label>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
    <textarea id="[[!+name]]" name="[[!+name]]" class="form-control form-control--textarea [[!+required:notempty=`required`:empty=``]] [[!+error:notempty=`error`]]" [[!+required:notempty=`required`:empty=``]] [[!+placeholder:notempty=`placeholder="[[!+placeholder]]"`]]>[[!+value]]</textarea>
    [[!+error]]
</div>
