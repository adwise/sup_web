<div class="field">
    <[[+property]] class="heading-l">[[+title]]</[[+property]]>
    [[!+description:notempty=`<div class="form-control--description">[[!+description]]</div>`]]
    <input type="hidden" id="[[!+name]]" name="[[!+name]]" value="-" />
</div>
