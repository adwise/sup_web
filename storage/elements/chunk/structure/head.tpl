<head>
	{$_modx->config['adw.meta_copyright']}

	<style>
		{ignore}html{visibility: visible}{/ignore}
	</style>

	{if $_modx->context.seo_google_gtm != 'GTM-ADWISE'}
		<script type="text/javascript" charset="utf-8" data-skip="1">

		</script>
	{/if}

	{$_modx->getChunk('seo_organization')}
	{$_modx->getChunk('seo_breadcrumbs')}
	{$_modx->getChunk('seo_links')}

	<base href="{$_modx->config.site_url}"/>
	<title>{$_modx->config['adw.seo_meta_title']}</title>
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta charset="utf-8"/>

	<meta name="description" content="{$_modx->resource.description?: $_modx->config['adw.seo_meta_description']}"/>
	<meta name="author" content="{$_modx->config['adw.seo_meta_author']}"/>
	<meta name="robots" content="{$_modx->config['adw.seo_meta_robots']?: $_modx->config['adw.seo_meta_robots']}">

	<meta property="og:locale" content="{$_modx->config.locale|replace:".utf8":""}"/>

	<meta property="og:type" content="{if $_modx->config.site_start == $_modx->resource.id}website{else}article{/if}"/>
	<meta property="og:title" content="{$_modx->config['adw.seo_meta_title']}"/>
	<meta property="og:description" content="{$_modx->resource.description?: $_modx->config['adw.seo_meta_description']}"/>
	<meta property="og:url" content="{$_modx->resource.id | url}"/>
	<meta property="og:site_name" content="{$_modx->config.site_name}"/>
	<meta property="og:image" content="{'ImagePlus' | snippet : [ 'value' => $_modx->resource.media_overview_image? : '/static/default/media/images/templating/social/logo-share.jpg', 'options' => 'w=1200&h=628&zc=1' ]}"/>

	<link rel="apple-touch-icon" sizes="180x180" href="/static/default/media/icons/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/static/default/media/icons/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/static/default/media/icons/favicon/favicon-16x16.png">
	<link rel="manifest" href="/static/default/media/icons/favicon/site.webmanifest">
	<link rel="mask-icon" href="/static/default/media/icons/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">


	<link rel="stylesheet" href="{$_modx->config['adw.url_static']}default/style/screen.min.css?v={$_modx->config['adw.general-release-version']}"/>

</head>


