<header class="header">
    <div class="grid-container">
        <div class="menu-desktop grid-x">
            <div class="logo">
                <a href="{$_modx->config.site_url}">
                    <img src="/static/default/media/images/templating/svg/logo-adwise-white.svg" alt="">
                </a>
            </div>
            <nav class="navigation" role="navigation">
                {'pdoMenu' | snippet : [
                'startId' => 0,
                'level' => 2,
                'outerClass' => 'mainnav grid-x no-bullet',
                'innerClass' => 'submenu no-bullet',
                'parentClass' => 'has-sub'
                ]}
            </nav>
        </div>
    </div>
</header>