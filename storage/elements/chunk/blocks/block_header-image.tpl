<section class="block_header-image">
    <div class="gradient"></div>
    <div class="bg-image show-for-xlarge"
         style="background-image: url({'ImagePlus' | snippet : ['value' => $block.image, 'options' => 'w=1600&zc=1&q=90']});"></div>
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell xlarge-6">
                <a class="title_pre" target="_blank" href="{$block.title_pre_href|inOrExternalUrl}">{$block.title_pre}</a>
                <h1 class="heading-h1 ">{$block.title}<span>.</span></h1>

                {if $block.image != ''}
                    <figure class="hide-for-xlarge">
                        {'ImagePlus' | snippet : ['type'=>'tpl','value' => $block.image, 'options' => 'w=640&zc=1&q=90']}
                    </figure>
                {/if}

                {$block.text}
                {if $block.link_href != ''}
                    <a href="{$block.link_href | inOrExternalUrl}" class="button button--primary"
                       title="{$block.link_caption}">{$block.link_caption}</a>
                {/if}
            </div>
        </div>
    </div>
</section>