<section class="block_forms">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12 small-offset-0 xlarge-10 xlarge-offset-1">
                {if $block.title}
                <h2 class="heading-xxl">{$block.title}</h2>
                {/if}
                <div class="grid-x block_forms-outer">
                    <div class="cell small-12 small-offset-0 xlarge-10 xlarge-offset-1">
                {$block.text}
                [[!FormaliciousRenderForm? &form=`{$block.form}`]]
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>