[[!FormIt@contact?
    &hooks=`csrf.hook,fiProcessArrays,fiGenerateReport,recaptchav3,email,redirect`
    &figrExcludedFields=`ghost,csrf,form-contact,savedForm_values,savedFormHashKey,recaptcha-token,recaptcha-action`
    &figrTpl=`email_figrTpl`
    &redirectTo=`[[++contactform_redirect-resource]]`
]]


<div class="alert alert-error">[[!+fi.validation_error_message]]  <pre>[[!+fi.error_message]]</pre></div>
<form method="post" action="" class="validate contact-form [[!+fi.validation_error_message:notempty=`error`]]" accept-charset="utf-8" enctype="application/x-www-form-urlencoded" id="contact-form">

    <div class="field [[!+fi.error.firstname:notempty=`error`]]">
        <label for="form-firstname" class="required">[[%Firstname]]</label>
        <input type="text" name="firstname" id="form-firstname" class="required" value="[[!+fi.firstname:esc]]" />
        [[!+fi.error.firstname]]
    </div>

    <div class="field [[!+fi.error.lastname:notempty=`error`]]">
        <label for="form-lastname" class="required">[[%Lastname]]</label>
        <input type="text" name="lastname" id="form-lastname" class="required" value="[[!+fi.lastname:esc]]" />
        [[!+fi.error.lastname]]
    </div>

    <div class="field [[!+fi.error.email:notempty=`error`]]">
        <label for="form-email" class="required">[[%Email]]</label>
        <input type="text" name="email" id="form-email" class="required" value="[[!+fi.email:esc]]" />
        [[!+fi.error.email]]
    </div>

    <div class="field [[!+fi.error.telephone:notempty=`error`]]">
        <label for="form-telephone">[[%Telephone]]</label>
        <input type="text" name="telephone" id="form-telephone" value="[[!+fi.telephone:esc]]" />
        [[!+fi.error.telephone]]
    </div>

    <div class="field [[!+fi.error.text:notempty=`error`]]">
        <label for="form-text" class="required">[[%Text]]</label>
        <textarea name="text" class="required">[[!+fi.text:striptags:esc]]</textarea>
        [[!+fi.error.text]]
    </div>

    [[!recaptchav3_render]]
    [[!+fi.error.recaptchav3_error]]

    <button>[[%Send]]</button>

    <input type="hidden" name="form-contact" value="Send" />
    <input type="hidden" name="ghost" value="" />
    <input type="hidden" name="csrf" value="[[!csrf.generate]]" />
    <input type="hidden" name="referral URL" value="[[~[[*id]]? &scheme=`full`]]"/>
    <input type="hidden" name="referral ID" value="[[*id]]"/>
</form>
