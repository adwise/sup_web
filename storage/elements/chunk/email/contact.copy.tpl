[[$email.wrapper
	?subject=`Kopie van uw contactaanvraag`
	&content=`<h2>Kopie van uw contactaanvraag</h2>

	<p>Op [[+adw.unix:date=`%d-%m-%Y`]] om [[+adw.unix:date=`%H:%M`]] heeft u een contactverzoek naar ons via onze <a href="[[++site_name]]" target="_blank" style="color: orange; text-decoration: none;">website</a> verzonden en aangegeven een kopie te willen ontvangen. Het verzoek is als volgt:</p>

	<table>
		<tr>
			<td width="200"><strong>Naam:</strong></td>
			<td>[[+firstname]] [[+lastname]]</td>
		</tr>
		<tr>
			<td width="200"><strong>E-mailadres:</strong></td>
			<td><a href="mailto:[[+email]]" target="_blank" style="color: orange; text-decoration: none;">[[+email]]</a></td>
		</tr>
		[[+telephone:notempty=`<tr>
			<td width="200"><strong>Telefoon:</strong></td>
			<td><span class="mobile_link">[[+telephone]]</span></td>
		</tr>`]]
	</table>

	<h3>Het bericht is als volgt:</h3>

	<p>[[+text:striptags:nl2br]]</p>
`]]