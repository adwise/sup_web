<script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "address": {
                "@type": "PostalAddress",
                "addressLocality": "{$_modx->config['adw.contact_city']}",
                "addressRegion": "{$_modx->config['adw.contact_state']}",
                "postalCode": "{$_modx->config['adw.contact_postalcode']}",
                "streetAddress": "{$_modx->config['adw.contact_address']}"
            },
            "name": "{$_modx->context.site_name}",
            "telephone": "{$_modx->config['adw.contact_telephone_general']}",
            "url": "{$_modx->config.site_url}"
        }
</script>