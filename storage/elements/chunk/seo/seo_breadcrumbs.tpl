<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "description":"Breadcrumbs list",
        "name":"Breadcrumbs",
        "itemListElement": [
            {'pdoCrumbs' | snippet :[
                'showHome' => '1',
                'tpl' => 'seo_breadcrumbs-row',
                'tplWrapper' => '@INLINE [[+output]]',
                'tplCurrent'=>'seo_breadcrumbs-current'
            ]}
        ]
    }
</script>