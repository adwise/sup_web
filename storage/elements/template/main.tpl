<!DOCTYPE html>
<html lang="{$_modx->config.cultureKey ?: 'nl'}" xml:lang="{$_modx->config.cultureKey ?: 'nl'}">
{block 'head'}
    {include 'file:chunk/structure/head.tpl'}
{/block}
<body>
{if $_modx->config.seo_google_gtm != 'GTM-ADWISE'}
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id={$_modx->config.seo_google_gtm}" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
{/if}

    <div class="off-canvas-wrapper">
        {block 'header'}
            {include 'file:chunk/structure/header.tpl'}
        {/block}

        {var $mobile = $_modx->getPlaceholder['adw.mobile'] == 1 ? 'show_mobile' : 'show_desktop'}
        {foreach json_decode($_modx->resource.migx_holder, true) as $block}
            {if $.php.file_exists($_modx->config.elements_path ~ '/chunk/blocks/' ~ $block.MIGX_formname ~ '.tpl') && $block[$mobile] == 1}
                {include ('file:chunk/blocks/' ~ $block.MIGX_formname ~ '.tpl') block=$block}
            {/if}
        {/foreach}

        {block 'footer'}
            {include 'file:chunk/structure/footer.tpl'}
        {/block}
    </div>

{block 'script'}
    {include 'file:chunk/structure/script.tpl'}
{/block}

<!-- {if $_modx->hasSessionContext('mgr')} {$_modx->getInfo()} {/if} -->
</body>
</html>
