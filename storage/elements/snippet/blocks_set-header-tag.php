<?php
if ($modx->getPlaceholder('blockheader') == '') {
    $blockheader = 1;
} else {
    $blockheader = 2;
}

$modx->toPlaceholder('blockheader', $blockheader);

return;