<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

//require_once dirname(__FILE__) . '/config.core.php';
//require_once MODX_CORE_PATH . 'model/modx/modx.class.php';

$modx = new modX();
$modx->initialize('mgr');

if (!$modx->getAuthenticatedUser('mgr')) {
	echo "Not logged in to manager";
	exit;
}


if (!isset($_GET['from']) || !isset($_GET['to'])) {
	echo "Define 'from' and 'to' contexts";
	exit;
}

$context_from = $_GET['from'];
$context_to = $_GET['to'];

echo '<pre>';

echo '-------------------------------------------------------------<br/>';
echo '--------------- CONVERT CONTEXT SETTINGS --------------------<br/>';
echo '-------------------------------------------------------------<br/>';

$settings = $modx->getIterator(
	'modContextSetting',
	array(
		array(
			'area:IN' => array(
				'Resources',
				'Discuss',
				'Navigation'
			),
			array(
				'OR:key:IN' => array(
					'site_start',
					'error_page',
					'site_unavailable_page',
					'unauthorized_page'
				)
			)
		),
		'context_key:=' => $context_to
	)
);

foreach($settings as $setting) {
	$resourceIDs = explode(',', $setting->get('value'));
	$newResourceIDs = array();

	foreach($resourceIDs as $resourceID) {
		if (!is_numeric($resourceID))
			continue;

		$newResourceID = $resourceID;

		$resource = $modx->getObject(
			'modResource',
			array(
				'id:=' => trim($resourceID),
				'context_key:=' => $context_from
			)
		);
		if ($resource) {
			$newResource = findResource($resource, $context_to);

			if ($newResource) {
				echo $resourceID . ': ' . $resource->get('uri') . ' -> ' . $newResource->get('id') . ': ' . $newResource->get('uri') . '<br/>';
				$newResourceID = $newResource->get('id');
			}
			else {
				echo $resourceID . ': ' . $resource->get('uri') . ' -> NEW RESOURCE NOT FOUND!!!!!<br/>';
			}
		}
		else {
			$newResource = $modx->getObject(
				'modResource',
				array(
					'id:=' => trim($resourceID),
					'context_key:=' => $context_to
				)
			);
			if ($newResource)
				echo $resourceID . ': ALREADY CONVERTED!!!!! (' . $newResource->get('id') . ': ' . $newResource->get('uri') . ')<br/>';
			else
				echo $resourceID . ': ALREADY CONVERTED!!!!! (COULD NOT FIND NEW RESOURCE?!?!?)<br/>';
		}
		$newResourceIDs[] = $newResourceID;
	}

	$setting->set('value', implode(',', $newResourceIDs));
	$setting->save();
}

echo '-------------------------------------------------------------<br/>';
echo '--------------------- FIX WEB LINKS -------------------------<br/>';
echo '-------------------------------------------------------------<br/>';

$weblinks = $modx->getIterator(
	'modResource',
	array(
		'class_key:=' => 'modWebLink',
		'context_key:=' => $context_to
	)
);

foreach($weblinks as $weblink) {
	if (is_numeric($weblink->get('content'))) {
		$resource = $modx->getObject(
			'modResource',
			array(
				'id:=' => trim($weblink->get('content')),
				'context_key:=' => $context_from
			)
		);

		if ($resource) {
			$toResource = findResource($resource, $context_to);

			if ($toResource) {
				$weblink->set('content', $toResource->get('id'));
				$weblink->save();

				echo $weblink->get('pagetitle') . ' -> ' . $toResource->get('menuindex') . '<br/>';
			}
		}
	}
}

$babelTvName = $modx->getOption('babel.babelTvName');

echo '-------------------------------------------------------------<br/>';
if ($babelTvName && $babelTvName != '') {
	echo '------------ FIX MENU ORDER & SET BABEL-LINKS ---------------<br/>';
}
else {
	echo '--------------------- FIX MENU ORDER ------------------------<br/>';
}
echo '-------------------------------------------------------------<br/>';

$resources = $modx->getIterator('modResource', array('context_key:=' => $context_to));
foreach($resources as $resource) {
	$fromResource = findResource($resource, $context_from);

	if ($fromResource) {
		if ($babelTvName && $babelTvName != '') {
			$fromBabelLinks = $fromResource->getTVValue($babelTvName);

			$fromBabelLinksArray = explode(';', $fromBabelLinks);
			foreach($fromBabelLinksArray as $key => $value) {
				$mapping = explode(':', $value);
				if ($mapping[0] == $context_to || $mapping[0] == $context_from)
					unset($fromBabelLinksArray[$key]);
			}
			$fromBabelLinksArray[] = $context_from . ':' . $fromResource->get('id');
			$fromBabelLinksArray[] = $context_to . ':' . $resource->get('id');

			$resource->setTVValue($babelTvName, implode(';', $fromBabelLinksArray));
			$fromResource->setTVValue($babelTvName, implode(';', $fromBabelLinksArray));
		}

		$resource->set('menuindex', $fromResource->get('menuindex'));
		$resource->save();

		echo $resource->get('pagetitle') . ' -> ' . $fromResource->get('menuindex') . ($fromBabelLinksArray ? ' (babel -> ' . implode(
					';',
					$fromBabelLinksArray
				) . ')' : '') . '<br/>';
	}
}

echo '-------------------------------------------------------------<br/>';
echo '-------------------- FIX TEMPLATE VARS ----------------------<br/>';
echo '-------------------------------------------------------------<br/>';

$sourceElements = $modx->getIterator('sources.modMediaSourceElement', array('context_key:=' => $context_from));
foreach($sourceElements as $sourceElement) {
	$newSourceElement = $modx->getObject(
		'sources.modMediaSourceElement',
		array(
			'object:=' => $sourceElement->get('object'),
			'object_class:=' => $sourceElement->get('object_class'),
			'context_key:=' => $context_to
		)
	);
	if (!$newSourceElement) {
		$newSourceElement = $modx->newObject('sources.modMediaSourceElement');
		$newSourceElement->fromArray(
			array(
				'object' => $sourceElement->get('object'),
				'object_class' => $sourceElement->get('object_class'),
				'context_key' => $context_to,
				'source' => $sourceElement->get('source')
			),
			'',
			true,
			true
		);
		$newSourceElement->save();
	}
}

$modx->cacheManager->refresh();

echo '</pre>';

function findResource($resource, $context) {
	global $modx;

	$newResource = $modx->getObject(
		'modResource',
		array(
			'pagetitle:=' => $resource->get('pagetitle'),
			'longtitle:=' => $resource->get('longtitle'),
			'menutitle:=' => $resource->get('menutitle'),
			'description:=' => $resource->get('description'),
			'introtext:=' => $resource->get('introtext'),
			'content:=' => $resource->get('content'),
			'published:=' => $resource->get('published'),
			'isfolder:=' => $resource->get('isfolder'),
			'class_key:=' => $resource->get('class_key'),
			'context_key:=' => $context
		)
	);

	return $newResource;
}