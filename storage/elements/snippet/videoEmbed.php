<?php
$parts   = parse_url($input);
$videoId = basename($parts['path']);

parse_str($parts['query'], $query);

if (!empty($query['v'])) {
    $videoId = $query['v'];
}

if (strpos($parts['host'], 'vimeo') !== false OR strpos($parts['host'], 'youtube') !== false) {
    if (is_numeric($videoId)) {
        $input   = 'vimeo';
    } else {
        $input   = 'youtube';
    }
}


if (strpos($input, 'vimeo') !== false) {
    $url = 'https://player.vimeo.com/video/'.$videoId;
}

if (strpos($input, 'youtube') !== false) {
    $url = 'https://www.youtube.com/embed/'.$videoId;
}

$modx->toPlaceholder('video_provider', $input);
$modx->toPlaceholder('video_id', $videoId);
$modx->toPlaceholder('video_url', $url);

return;