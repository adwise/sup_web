<?php
$resourceId = $modx->resource->get('id');
$fullURL    = $modx->makeUrl($resourceId, "", "", "full");

if ($input) {
    if (is_numeric($input)) {
        return $modx->makeUrl($input, '', '', 'full');
    } else {
// if input has full url
        if (preg_match('#^http?.:\/\/#i', $input) === 1) {
            return $input;
        }

// return input with pre the full url
        return $fullURL.$input;
    }
}

// if empty return full url
return $fullURL;