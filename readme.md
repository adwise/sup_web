# MODx default

## Getting started
Going trough these instructions will set up a local copy of the "modx default" environment. This readme encompasses the set-up of the docker environment.

### Prerequisites
In order to run this docker environment the following requirements must be met:

* [Docker](https://docs.docker.com/install/)
* [Docker Compose](https://docs.docker.com/compose/install/) (included in Mac and Windows docker installs)
* Your [SSH key is registered with bitbucket](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html)

### Installing

Clone this project
```
git clone git@bitbucket.org:adwise/modx_default.git
```

Navigate to the project folder and start docker
```
cd modx_default
docker-compose up -d
```

Copy example config in the `core/config` directory to `config.inc.php`.

Update the database parameters to your environment
```
$database_server = '%DATABASE-HOST%';
$database_user = '%DATABASE-USER%';
$database_password = '%DATABASE-PASSWORD%';
$dbase = '%DATABASE-NAME%';
```

Update the environment system-setting values. For local usage these can be set to
```
$config_options = [
    'server_protocol'   => 'http',
    'adw.htaccess_ssl'  => '0',
    'adw.domain_suffix' => '.local',
    'mail_use_smtp' => '1',
    'mail_smtp_port' => '1025', //use port 1025 for local environments and 587 for online locations
    'mail_smtp_hosts' => 'mailhog',
                                  
    'elements_path' => dirname(dirname(dirname(__FILE__))) . '/storage/elements/'
];
```

SSH into the container and install composer dependencies
```
docker-compose exec default_php /bin/bash
composer install
```

Add this to your /etc/hosts (if you aren't using something like dnsmasq):
```
127.0.0.1       	domainnamehere.com.local
```

The project will now be available at [modx-default.nl.local]

### Post install

#### Generate .htaccess files

Rename the ht.access file in the public_html folder to .htaccess

Login in the [manager](http://domainnamehere.com.local/manager/) and clear the cache by clicking the menu-item
```
Adwise logo > Update .htaccess Files
```

It will auto-update the ```.htaccess``` files

### For live sites
Download the last version of the database from 
```
Server1 (31.7.6.85)
User : modx
```